/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import ws.model.Procedure;
import ws.db.DatabaseOperations;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import oracle.jdbc.OracleCallableStatement;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ws.db.DatabaseOperations;

/**
 *
 * @author EXT0126117
 */
public class WSGenerator {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        Connection conn = null;
        DatabaseOperations databaseOperations = new DatabaseOperations("APPS", "devapps#490", "panda.erp.mis.turkcell.tgc", "1533", "TDEV");
        WSUtils utils = new WSUtils();
        String packageName = "XXTG_WS_GENERATOR_TEST_PKG";
        String dataSourceName = "EF_DS";
        try 
        {
            conn = databaseOperations.getConnection();

            System.out.println("Database connection opened!");
            

            String pksScript = utils.getPksScript(conn, packageName);
            String pkbScript = null;//utils.getPkbScript(conn, packageName);
            System.out.println(pksScript);
            
            List<Procedure> procedures = new ArrayList<Procedure>();
            procedures = utils.getProcedures(conn, pksScript, pkbScript);
            
            boolean appsHeaderFlag = true;
            utils.createFiles(procedures, packageName, dataSourceName, appsHeaderFlag);
            
            
            conn.close();
            System.out.println("Database connection closed!");
        }catch(SQLException exp)
        {
            System.out.println("Database Error: "+exp.toString());
            conn = null;
        }catch(IOException exp)
        {
            System.out.println(exp.toString());
        }
        
    }
    
    public void generateFiles(List<Procedure> procedures, String packageName, String dataSourceName, boolean appsHeaderFlag)
    throws IOException
    {
        WSUtils utils = new WSUtils();
        utils.createFiles(procedures, packageName, dataSourceName, appsHeaderFlag);
    }
}
