/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import ws.model.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.antlr.stringtemplate.*;
import ws.db.DatabaseOperations;
import ws.file.FileUtils;
import ws.template.*;

/**
 *
 * @author EXT0126117
 */
public class WSUtils {
    
    public void savePropertiesFile(String userDB, String passwordDB, String hostDB, String portDB, String sidDB) throws IOException
    {
        FileUtils fileUtils = new FileUtils();
        String content = fileUtils.getPropertiesContent(userDB, passwordDB, hostDB, portDB, sidDB);
        fileUtils.createPropertiesFile(fileUtils.getPropertiesFileName(), fileUtils.getPropertiesFileExteintion(), content);
    }
    
    public String getPksScript(Connection conn, String packageName) throws SQLException, IOException{
        DatabaseOperations databaseOperations = new DatabaseOperations();
        Clob pksScriptClob = databaseOperations.getPksScriptFromDb(conn, packageName);
        //System.out.println("pksScriptClob: "+pksScriptClob);
        String pksScript = clobToString(pksScriptClob);
        //System.out.println("pksScript: "+pksScript);
        pksScript = clearComments(pksScript);
        pksScript = pksScript.replaceAll("\\s+", " ").trim();
        
        return pksScript;
    }
    
    public String getPkbScript2(Connection conn, String packageName) throws SQLException, IOException{
        DatabaseOperations databaseOperations = new DatabaseOperations();
        Clob pkbScriptClob = databaseOperations.getPkbScriptFromDb(conn, packageName);
        //System.out.println("pksScriptClob: "+pksScriptClob);
        String pkbScript = clobToString(pkbScriptClob);
        //System.out.println("pksScript: "+pksScript);
        pkbScript = clearComments(pkbScript);
        pkbScript = pkbScript.replaceAll("\\s+", " ").trim();
        return clearComments(pkbScript);
    }
    
    public String getTypeScript(Connection conn, String typeName, String pksScript) throws SQLException, IOException{
        if(typeName.indexOf(".") >= 0)
        {
            typeName = typeName.substring(typeName.indexOf(".")+1);
        }
        if(typeName.indexOf(";") >= 0)
        {
            typeName = typeName.substring(0, typeName.indexOf(";"));
        }
        
        DatabaseOperations databaseOperations = new DatabaseOperations();
        Clob typeScriptClob = databaseOperations.getTypeScriptFromDb(conn, typeName);
        System.out.println("typeScriptClob: "+typeScriptClob);
        String pksScriptTemp = null;
        String typeScript = null;
        
        typeName = typeName.toLowerCase();
        if(typeScriptClob != null)
        {
            typeScript = clobToString(typeScriptClob);
        }else
        {
            if(pksScript.indexOf(" type "+typeName+" is record") >= 0)
            {
                pksScriptTemp = pksScript.substring(pksScript.indexOf(" type "+typeName+" is record"), pksScript.indexOf(";", pksScript.indexOf(" type "+typeName+" is record")));
                typeScript = pksScriptTemp.trim();
            }else if(pksScript.indexOf(" type "+typeName+" is ref cursor") >= 0)
            {
                pksScriptTemp = pksScript.substring(pksScript.indexOf(" type "+typeName+" is ref cursor"), pksScript.indexOf(";", pksScript.indexOf(" type "+typeName+" is ref cursor")));
                typeScript = pksScriptTemp.trim();
            }else
            {
                typeScript = typeName;
            }
        }
        System.out.println("typeName1: "+typeName);
        return clearComments(typeScript);
    }
    
    public void createFiles(List<Procedure> procedures, String packageName, String dataSourceName, boolean appsHeaderFlag)
    throws IOException
    {
        FileUtils fileUtils = new FileUtils();
        fileUtils.deleteAll();
        fileUtils.createHandlerChainFile(packageName);
        fileUtils.createWebServiceFile(appsHeaderFlag, packageName.toUpperCase(), packageName.toUpperCase(), dataSourceName, procedures);
        
        for (Procedure procedure : procedures) {
            // create request, response files for each one procedure
            fileUtils.createRequestResponseFile(appsHeaderFlag, packageName.toUpperCase(), procedure.getProcedureName().toUpperCase(), procedure.getParamters());
            
            fileUtils.createTypeFiles(procedure, packageName.toUpperCase());
        }
    }
    
    
    
    public String formatName (String name)
    {
        String formattedParameter = name;
        if (name != null && !"".equals(name))
        {
            
            if (name.indexOf("p_") == 0 || name.indexOf("x_") == 0)
            {
                 formattedParameter = name.substring(2, name.length());
            }

            return formattedParameter.toUpperCase();
        }
            
        return formattedParameter;
    }
    
    public String formatTypeJava (Connection conn, String type, String pksScript)
    throws SQLException
    {
        DatabaseOperations databaseOperations = new DatabaseOperations();
        String formattedParameterType = type;
        
        if (type != null && !"".equals(type))
        {
            if("number".equals(type))
            {
                formattedParameterType = "BigDecimal";
            }else if(type.contains("varchar"))
            {
                formattedParameterType = "String";
            }else if("clob".equals(type))
            {
                formattedParameterType = "CLOB";
            }else if("blob".equals(type))
            {
                formattedParameterType = "byte[]";
            }else if("date".equals(type))
            {
                formattedParameterType = "Date";
            }else if("sys_refcursor".equals(type))
            {
                formattedParameterType = "sys_refcursor";
            }else if(databaseOperations.isType(conn, type))
            {
                formattedParameterType = "Object";
            }else if(isRefCursor(conn, type, pksScript))
            {
                formattedParameterType = "refcursor";
            }
        }
        
        return formattedParameterType;
    }
    
    public String formatTypeJava2 (Connection conn, String type)
    throws SQLException
    {
        DatabaseOperations databaseOperations = new DatabaseOperations();
        String formattedParameterType = type;
        
        if (type != null && !"".equals(type))
        {
            if("number".equals(type))
            {
                formattedParameterType = "BigDecimal";
            }else if(type.contains("varchar"))
            {
                formattedParameterType = "String";
            }else if("clob".equals(type))
            {
                formattedParameterType = "CLOB";
            }else if("blob".equals(type))
            {
                formattedParameterType = "byte[]";
            }else if("date".equals(type))
            {
                formattedParameterType = "";
            }else if("ref cursor".equals(type))
            {
                formattedParameterType = "sys_refcursor";
            }else if("object".equals(type))
            {
                formattedParameterType = "Object";
            }/*else if(databaseOperations.isType(conn, type))
            {
                formattedParameterType = "Object";
            }*//*else if(isRefCursor(conn, type, pksScript))
            {
                formattedParameterType = "refcursor";
            }*/
        }
        
        return formattedParameterType;
    }
    
    public boolean isRefCursor(Connection conn, String type, String pksScript)
    {
        boolean returnValue = false;

        if(pksScript.indexOf(" type "+type+" is ref cursor") >= 0)
        {
            returnValue = true;
        }
                
        return returnValue;
    }
    
    public String formatTypeJavaMethod (String type)
    {
        String formattedParameterType = type;
        
        if (type != null && !"".equals(type))
        {
            if ("byte[]".equals(type))
            {
                formattedParameterType = "Bytes";
            }
        }
        
        return formattedParameterType;
    }
    
    public String formatNameJavaMethod (String name)
    {
        
        return name.substring(0,1).toUpperCase()+name.toLowerCase().substring(1, name.length());
    }
    
    public List<Type> getTypeList(List<Type> typeScripts, Parameter parameter, int index, Connection conn, String typeName, String pksScript, String columnName)
    throws SQLException, IOException
    {
        DatabaseOperations databaseOperations = new DatabaseOperations();
        String typeScript = getTypeScript(conn, typeName, pksScript);
        
        while(true)
        {
            Type  newListItem = new Type();

            if(typeScript.contains("is table of") || typeScript.contains("as table of"))
            {
                index++;
                newListItem.setIndex(index);
                newListItem.setScript(typeScript);
                newListItem.setTypeName(getTypeName(typeScript));
                if(columnName == null)
                {
                    newListItem.setTypeColumns(getTypeColumns(conn, parameter.getParameterName().toUpperCase(), null, typeScript, pksScript));
                }else
                {
                    newListItem.setTypeColumns(getTypeColumns(conn, parameter.getParameterName().toUpperCase(), columnName, typeScript, pksScript));
                }
                newListItem.setTypeClass("TABLE");
                newListItem.setCustomType(databaseOperations.isType(conn, newListItem.getTypeName()));
                typeScripts.add(newListItem);
                typeScript = getTypeScript(conn, findChildParameterType(typeScript).toUpperCase(), pksScript);
            }else if(typeScript.contains("is ref cursor"))
            {
                index++;
                newListItem.setIndex(index);
                newListItem.setScript(typeScript);
                newListItem.setTypeName(getTypeName(typeScript));
                newListItem.setTypeColumns(getTypeColumns(conn, parameter.getParameterName().toUpperCase(), null, typeScript, pksScript));
                newListItem.setTypeClass("TABLE");
                newListItem.setCustomType(true);
                typeScripts.add(newListItem);
                typeScript = getTypeScript(conn, findChildParameterType(typeScript).toUpperCase(), pksScript);
            }else if(typeScript.contains("is record"))
            {
                index++;
                newListItem.setIndex(index);
                newListItem.setScript(typeScript);
                newListItem.setTypeName(getTypeName(typeScript));
                newListItem.setTypeColumns(getTypeColumns(conn, parameter.getParameterName().toUpperCase(), null, typeScript, pksScript));
                newListItem.setTypeClass("RECORD");
                newListItem.setCustomType(databaseOperations.isType(conn, newListItem.getTypeName()));
                typeScripts.add(newListItem);
                //typeScript = getTypeScript(conn, findChildParameterType(typeScript).toUpperCase(), pksScript);
                if(!isTypeExists(conn, newListItem.getTypeColumns(), pksScript))
                {
                    break;
                }else
                {
                    for (TypeColumn typeColumn : newListItem.getTypeColumns()) {
                        String formatedParameterTypeJava = formatTypeJava (conn, typeColumn.getColumnType(), pksScript);
                        if("Object".equals(formatedParameterTypeJava))
                        {
                            typeScript = getTypeScript(conn, typeColumn.getColumnType().toUpperCase(), pksScript);
                            getTypeList(typeScripts, parameter, index, conn, typeColumn.getColumnType().toUpperCase(), pksScript, typeColumn.getColumnName());
                        }
                    }
                    break;
                }
            }else
            {
                index++;
                newListItem.setIndex(index);
                newListItem.setScript(typeScript);
                newListItem.setTypeName(getTypeName(typeScript));
                newListItem.setTypeColumns(getTypeColumns(conn, null, null, typeScript, pksScript));
                newListItem.setTypeClass("RECORD");
                newListItem.setCustomType(databaseOperations.isType(conn, newListItem.getTypeName()));
                typeScripts.add(newListItem);
                
                if(!isTypeExists(conn, newListItem.getTypeColumns(), pksScript))
                {
                    break;
                }else
                {
                    for (TypeColumn typeColumn : newListItem.getTypeColumns()) {
                        String formatedParameterTypeJava = formatTypeJava (conn, typeColumn.getColumnType(), pksScript);
                        if("Object".equals(formatedParameterTypeJava))
                        {
                            typeScript = getTypeScript(conn, typeColumn.getColumnType().toUpperCase(), pksScript);
                            getTypeList(typeScripts, parameter, index, conn, typeColumn.getColumnType().toUpperCase(), pksScript, typeColumn.getColumnName());
                        }
                    }
                    break;
                }
                
                
                //typeScript = getTypeScript(conn, newListItem.getTypeName());
                //getRecordTypes(typeScripts, parameter, index, conn, recordParameterType);
            }
            
            
        }
        
        return typeScripts;
    }
    
    public boolean isTypeExists(Connection conn, List<TypeColumn> typeColumns, String pksScript)
    throws SQLException
    {
        boolean returnValue = false;
        
        for (TypeColumn typeColumn : typeColumns) {
            String formatedParameterTypeJava = formatTypeJava (conn, typeColumn.getColumnType(), pksScript);
            if("Object".equals(formatedParameterTypeJava))
            {
                returnValue = true;
                break;
            }
        }
        
        return returnValue;
    }
    
    public String getFirstType(Connection conn, List<TypeColumn> typeColumns, String pksScript)
    throws SQLException
    {
        String returnValue = "-1";
        for (TypeColumn typeColumn : typeColumns) {
            String formatedParameterTypeJava = formatTypeJava (conn, typeColumn.getColumnType(), pksScript);
            if("Object".equals(formatedParameterTypeJava))
            {
                returnValue = typeColumn.getColumnType();
                break;
            }
        }
        
        return returnValue;
    }
    
    public String getTypeName(String script)
    {
        String typeName = "";
        
        if(script.indexOf("is table of") >= 0)
        {
            typeName = script.substring(script.indexOf("create or replace type")+"create or replace type".length(), script.indexOf("is table of")).trim();
            typeName = typeName.replace("\"", "");
            if(typeName.indexOf("apps.") >= 0)
            {
                typeName = clearApps(typeName);
            }
        }else if(script.indexOf("as table of") >= 0)
        {
            typeName = script.substring(script.indexOf("create or replace type")+"create or replace type".length(), script.indexOf("as table of")).trim();
            typeName = typeName.replace("\"", "");
            if(typeName.indexOf("apps.") >= 0)
            {
                typeName = clearApps(typeName);
            }
        }else if(script.indexOf("as object") >= 0)
        {
            script = script.replaceAll(" force ", " ");
            typeName = script.substring(script.indexOf("create or replace type")+"create or replace type".length(), script.indexOf("as object")).trim();
            typeName = typeName.replace("\"", "");
            if(typeName.indexOf("apps.") >= 0)
            {
                typeName = clearApps(typeName);
            }
        }else if(script.indexOf("is object") >= 0)
        {
            script = script.replaceAll(" force ", " ");
            typeName = script.substring(script.indexOf("create or replace type")+"create or replace type".length(), script.indexOf("is object")).trim();
            typeName = typeName.replace("\"", "");
            if(typeName.indexOf("apps.") >= 0)
            {
                typeName = clearApps(typeName);
            }
        }else if(script.indexOf("is ref cursor") >= 0)
        {
            //script = script.replaceAll(" force ", " ");
            typeName = script.substring(script.indexOf("type")+"type".length(), script.indexOf("is ref cursor")).trim();
            /*typeName = typeName.replace("\"", "");
            if(typeName.indexOf("apps.") >= 0)
            {
                typeName = clearApps(typeName);
            }*/
        }else if(script.indexOf("is record") >= 0)
        {
            //script = script.replaceAll(" force ", " ");
            typeName = script.substring(script.indexOf("type")+"type".length(), script.indexOf("is record")).trim();
            /*typeName = typeName.replace("\"", "");
            if(typeName.indexOf("apps.") >= 0)
            {
                typeName = clearApps(typeName);
            }*/
        }else
        {
            typeName = clearSize(script);
        }
        
        return typeName;
    }
    
    public String clear(String typeName)
    {
        String returnValue = clearApps(typeName);
        //clearApps(typeName);
        clearSize(typeName);
        
        if(returnValue.indexOf(";") >= 0)
        {
            returnValue = returnValue.substring(0, returnValue.indexOf(";"));
        }
        return returnValue;
    }
    
    public String clearApps(String typeName)
    {
        if(typeName.indexOf("apps.") >= 0)
        {
            return typeName.substring(typeName.indexOf("apps.")+"apps.".length(), typeName.length());
        }else
        {
            return typeName;
        }
    }
    
    public String clearSize(String columnType)
    {
        String columnTypeCleared = "";
        if(columnType.contains("varchar") && columnType.contains("("))
        {
            columnTypeCleared = columnType.substring(0, columnType.indexOf("("));
        }else if(columnType.contains("number("))
        {
            columnTypeCleared = columnType.substring(0, columnType.indexOf("("));
        }else
        {
            columnTypeCleared = columnType;
        }
        
        return columnTypeCleared;
    }
    
    public List<TypeColumn> getTypeColumns(Connection conn, String columnName, String columnName2, String script, String pksScript)
    throws SQLException, IOException
    {
        //System.out.println("script111: "+script);
        //System.out.println("pksScript111: "+pksScript);
        DatabaseOperations databaseOperations = new DatabaseOperations();
        List<TypeColumn> typeColums = new ArrayList<TypeColumn>();
        String firstScript = script;
        String columnText = "";
        if(script.indexOf("as object") >= 0)
        {
            script = script.substring(script.indexOf("as object")+"as object".length(), script.length()).trim();
        }else if(script.indexOf("is record") >= 0)
        {
            script = script.substring(script.indexOf("is record")+"is record".length(), script.length()).trim();
        }
        script = script.substring(script.indexOf("(")+1, script.length()).trim();
        boolean isLastItem = false;
        boolean isItemExists = script.indexOf(",") >= 0 || (firstScript.indexOf("as object") >= 0 && script.trim().length() > 0)  ? true : false;
        
        if (isItemExists)
        {
            while(true)
            {
                if (script.indexOf(",") >= 0 || isLastItem)
                {
                    TypeColumn  newListItem = new TypeColumn();
                    if (!isLastItem)
                    {
                        columnText = script.substring(0, script.indexOf(",")).trim();
                        script = script.substring(script.indexOf(",")+1, script.length());
                    }else
                    {
                        columnText = script.substring(0, script.indexOf(")")).trim();
                    }
                    
                    newListItem.setColumnName(columnText.substring(0, columnText.indexOf(" ")).trim());
                    newListItem.setColumnType(clear(clearSize(columnText.substring(columnText.indexOf(" "), columnText.length()).trim())));
                    newListItem.setColumnNameJava(formatName(newListItem.getColumnName().toLowerCase()));
                    newListItem.setColumnTypeJava(formatTypeJava(conn, newListItem.getColumnType(), pksScript));
                    newListItem.setColumnTypeJavaMethod(formatTypeJavaMethod(newListItem.getColumnTypeJava()));
                    newListItem.setColumnNameJavaMethod(formatNameJavaMethod(newListItem.getColumnNameJava()));
                    newListItem.setCustomType(databaseOperations.isType(conn, newListItem.getColumnType()));
                    newListItem.setColumnTypeClass(getColumnTypeClass(conn, newListItem.isCustomType(), newListItem.getColumnType()));
                    
                    typeColums.add(newListItem);
                    
                    if(isLastItem)
                    {
                        break;      
                    }
                }else
                {
                    isLastItem = true;
                }
            }
        }else if(firstScript.contains("is table of") || firstScript.contains("as table of"))
        {
            TypeColumn  newListItem = new TypeColumn();
            //newListItem.setColumnName(parameter.getParameterName());
            
            
            if(columnName2 == null)
            {
                newListItem.setColumnName(columnName);
                //newListItem.setColumnNameJava(formatName(newListItem.getColumnName().toLowerCase()+"_typ"));
            }else
            {
                newListItem.setColumnName(columnName2);
                //newListItem.setColumnNameJava(formatName(newListItem.getColumnName().toLowerCase()));
            }
            newListItem.setColumnNameJava(formatName(newListItem.getColumnName().toLowerCase()+"_typ"));
            newListItem.setColumnType(findChildParameterType(firstScript));
            newListItem.setColumnTypeJava(formatTypeJava(conn, newListItem.getColumnType(), pksScript));
            newListItem.setColumnTypeJavaMethod(formatTypeJavaMethod(newListItem.getColumnTypeJava()));
            newListItem.setColumnNameJavaMethod(formatNameJavaMethod(newListItem.getColumnNameJava()));
            newListItem.setCustomType(databaseOperations.isType(conn, newListItem.getColumnType()));
            newListItem.setColumnTypeClass(getColumnTypeClass(conn, newListItem.isCustomType(), newListItem.getColumnType()));
            typeColums.add(newListItem);
        }else
        {
            TypeColumn  newListItem = new TypeColumn();
            //newListItem.setColumnName(parameter.getParameterName());
            System.out.println("columnName000: "+columnName);
            System.out.println("columnName2000: "+columnName2);
            if(columnName == null)
            {
                newListItem.setColumnName("param");
            }else
            {
                newListItem.setColumnName(columnName);
            }
            newListItem.setColumnType(findChildParameterType(firstScript));
            System.out.println("1");
            System.out.println("newListItem.getColumnName(): "+newListItem.getColumnName());
            newListItem.setColumnNameJava(formatName(newListItem.getColumnName().toLowerCase()));
            System.out.println("2");
            newListItem.setColumnTypeJava(formatTypeJava(conn, newListItem.getColumnType(), pksScript));
            newListItem.setColumnTypeJavaMethod(formatTypeJavaMethod(newListItem.getColumnTypeJava()));
            newListItem.setColumnNameJavaMethod(formatNameJavaMethod(newListItem.getColumnNameJava()));
            newListItem.setCustomType(databaseOperations.isType(conn, newListItem.getColumnType()));
            newListItem.setColumnTypeClass(getColumnTypeClass(conn, newListItem.isCustomType(), newListItem.getColumnType()));
            typeColums.add(newListItem);
        }
        
        return typeColums;
    }
    
    public String getColumnTypeClass(Connection conn, boolean isCustomType, String columnType) throws SQLException, IOException
    {
        String columnTypeClass = "STANDART";
        if (isCustomType)
        {
            DatabaseOperations databaseOperations = new DatabaseOperations();
            Clob typeScriptClob = databaseOperations.getTypeScriptFromDb(conn, columnType.toUpperCase());
            if(typeScriptClob != null)
            {
                String typeScriptInt = clobToString(typeScriptClob);
                if(typeScriptInt.indexOf("as object") >= 0 || typeScriptInt.indexOf("is record") >= 0)
                {
                    columnTypeClass = "RECORD";
                }else if(typeScriptInt.contains("is table of") || typeScriptInt.contains("as table of"))
                {
                    columnTypeClass = "TABLE";
                }
            }
        }
        
        return columnTypeClass;
    }
    
    public String findChildParameterType(String typeScript)
    {
        String parameterName = "";
        if(typeScript.indexOf("is table of") >= 0)
        {
            parameterName = typeScript.substring(typeScript.indexOf("is table of")+"is table of".length(), typeScript.length()).trim();
        }else if(typeScript.indexOf("as table of") >= 0)
        {
            parameterName = typeScript.substring(typeScript.indexOf("as table of")+"as table of".length(), typeScript.length()).trim();
        }else if(typeScript.indexOf("is ref cursor") >= 0)
        {
            parameterName = typeScript.substring(typeScript.indexOf("return")+"return".length(), typeScript.length()).trim();
        }else if(typeScript.contains("varchar"))
        {
            parameterName = clearSize(typeScript);
        }else if(typeScript.contains("number"))
        {
            parameterName = clearSize(typeScript);
        }else if(typeScript.equals("blob"))
        {
            parameterName = "blob";
        }else if(typeScript.equals("clob"))
        {
            parameterName = "clob";
        }
        
        parameterName = clear(parameterName);
        
        return parameterName;
                
    }
    
    public String clobToString(Clob data) throws IOException, SQLException{
        StringBuilder sb = new StringBuilder();
        
            Reader reader = data.getCharacterStream();
            BufferedReader br = new BufferedReader(reader);

            String line;
            while(null != (line = br.readLine())) {
                sb.append(line.toLowerCase()).append("\n");
            }
            br.close();
        
        return sb.toString();
    }
    
    
    
    public List<Parameter> getParameters(Connection conn, String oneProcedureDefinition, String pksScript)
    throws SQLException, IOException
    {
        List<Parameter> parametersList = new ArrayList<Parameter>();
        //System.out.println("oneProcedureDefinition : "+oneProcedureDefinition);
        String parameters = oneProcedureDefinition.substring(oneProcedureDefinition.indexOf("(")+1, oneProcedureDefinition.indexOf(")"));
        //System.out.println(parameters);
        
        //parameters.
        while(true)
        {
                Parameter parameter = new Parameter();
                String paramaterTemp = "";
                if (parameters.indexOf(",") > 0)
                {
                    paramaterTemp = parameters.substring(0, parameters.indexOf(",")).trim();
                }else
                {
                    paramaterTemp = parameters.substring(0, parameters.length()).trim();
                }
                //System.out.println(paramaterTemp);
                if(paramaterTemp.indexOf(" default ")>=0)
                {
                    paramaterTemp = paramaterTemp.substring(0, paramaterTemp.lastIndexOf(" default ")).trim();
                }
                String parameterName = paramaterTemp.substring(0, paramaterTemp.indexOf(" ")).trim();
                //System.out.println(parameterName);
                
                String parameterType = paramaterTemp.substring(paramaterTemp.lastIndexOf(" ")+1, paramaterTemp.length()).trim();
                //System.out.println(parameterType);
                String parameterDirection = "";
                //System.out.println(paramater.substring(paramater.indexOf(" "), paramater.lastIndexOf(" ")));
                if (paramaterTemp.substring(paramaterTemp.indexOf(" "), paramaterTemp.lastIndexOf(" ")).contains("out"))
                {
                    parameterDirection = "out";
                }else
                {
                    parameterDirection = "in";

                }
                //System.out.println(parameterDirection);
                parameter.setParameterName(parameterName);
                parameter.setParameterType(parameterType);
                parameter.setParameterDirection(parameterDirection);
                parameter.setParameterNameJava(formatName(parameterName.toLowerCase()));
                parameter.setParameterTypeJava(formatTypeJava(conn, parameterType.toLowerCase(), pksScript));
                parameter.setParameterTypeJavaMethod(formatTypeJavaMethod(parameter.getParameterTypeJava()));
                parameter.setParameterNameJavaMethod(formatNameJavaMethod(parameter.getParameterNameJava()));
                if("Object".equals(parameter.getParameterTypeJava()))
                {
                    
                    //parameter.setTypes(getTypeList2(conn, parameter));
                    List<Type> typeScripts = new ArrayList<Type>();
                    int index = 0;
                    parameter.setTypes(getTypeList(typeScripts, parameter, index, conn, parameter.getParameterType().toUpperCase(), pksScript, null));

                }
                parametersList.add(parameter);

                if (parameters.indexOf(",") < 0)
                {
                    break;
                }
                parameters = parameters.substring(parameters.indexOf(",")+1, parameters.length()).trim();
                //System.out.println(parameters);
        }
        
        return parametersList;
    }
    
    public List<Parameter> getParameters2(Connection conn, String packageName, String pksScript)
    throws SQLException, IOException
    {
        DatabaseOperations databaseOperations = new DatabaseOperations();
        List<Parameter> parametersList = new ArrayList<Parameter>();
        List<ProcedureArgument> procedureArgumentsList = new ArrayList<ProcedureArgument>();

        Parameter parameter = new Parameter();

        procedureArgumentsList = databaseOperations.getPackageArguments(conn, packageName);

        for (ProcedureArgument procedureArguments : procedureArgumentsList) 
        {
            parameter.setParameterName(procedureArguments.getArgumentName());
            parameter.setParameterType(procedureArguments.getDataType());
            parameter.setParameterDirection(procedureArguments.getInOut());
            parameter.setParameterNameJava(formatName(procedureArguments.getArgumentName().toLowerCase()));
            parameter.setParameterTypeJava(formatTypeJava2(conn, procedureArguments.getDataType().toLowerCase()));
            parameter.setParameterTypeJavaMethod(formatTypeJavaMethod(parameter.getParameterTypeJava()));
            parameter.setParameterNameJavaMethod(formatNameJavaMethod(parameter.getParameterNameJava()));
            if("Object".equals(parameter.getParameterTypeJava()))
            {

                //parameter.setTypes(getTypeList2(conn, parameter));
                List<Type> typeScripts = new ArrayList<Type>();
                int index = 0;
                parameter.setTypes(getTypeList(typeScripts, parameter, index, conn, parameter.getParameterType().toUpperCase(), pksScript, null));

            }

            parametersList.add(parameter);
        }
        
        return parametersList;
    }
    
    
    
    public String clearComments(String script)
    {

        String filtered = "";//script.replaceAll("/\\*.*\\*/", " ");
        //System.out.println("filtered0: "+filtered);
        //System.out.println("fullFiltered: "+filtered);
        //String filtered = null;
        //if(script != null)
        //{
            Pattern pattern = Pattern.compile("(/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/)|(--.*)");
            System.out.println("script start");
            System.out.println(script);
            System.out.println("script end");
            Matcher matcher = pattern.matcher(script);
            System.out.println("matcher end");
            while(matcher.find()) {
                System.out.println(""+matcher.start());
                System.out.println(""+matcher.end());
                System.out.println(""+matcher.group());
            }
            filtered = matcher.replaceAll(" ");
            System.out.println("filtered1: "+filtered);

            /*pattern = Pattern.compile("(?i)[-]+.+");
            matcher = pattern.matcher(filtered);
            filtered = matcher.replaceAll(" ");
            //System.out.println("filtered2: "+filtered);
            while(matcher.find()) {
                System.out.println(""+matcher.start());
                System.out.println(""+matcher.end());
                System.out.println(""+matcher.group());
            }
            System.out.println("filtered2: "+filtered);*/
            return filtered;
        /*}else
        {
            return null;
        }*/
    }
       
    
    public List<Procedure> getProcedures(Connection conn, String pksScript, String pkbScript)
    throws SQLException, IOException
    {
        List<Procedure> proceduresList = new ArrayList<Procedure>();
        String proceduresDefinition = pksScript.substring(pksScript.indexOf("procedure"), pksScript.lastIndexOf("end")).trim();;
        System.out.println("1: ");
        String oneProcedureDefinition = "";

        int startIndex = 0;
        int endIndex = 0;
        //int firstStartIndex = 0;
        while(true)
        {
            //System.out.println("--------");
            if(proceduresDefinition.indexOf(";") < 0)
            {
                break;
            }
            oneProcedureDefinition = proceduresDefinition.substring(0, proceduresDefinition.indexOf(";")).trim();
            //System.out.println("oneProcedureDefinition1: "+oneProcedureDefinition);
            oneProcedureDefinition = oneProcedureDefinition.replace("(", " (");
            oneProcedureDefinition = oneProcedureDefinition.replace(")", " )");
            //System.out.println("oneProcedureDefinition2: "+oneProcedureDefinition);
            
            //System.out.println("cnt: "+cnt);
            if (oneProcedureDefinition.indexOf("procedure") >= 0)
            {
                startIndex = oneProcedureDefinition.indexOf("procedure")+"procedure".length()+1;
                endIndex = oneProcedureDefinition.indexOf(" ", startIndex);
                
                Procedure procedure = new Procedure();
                String nextProcedure = proceduresDefinition.substring(startIndex, endIndex).trim();
                procedure.setProcedureName(nextProcedure);
                System.out.println(nextProcedure);

                List<Parameter> parameterslist = new ArrayList<Parameter>();
                parameterslist = getParameters(conn, oneProcedureDefinition, pksScript);
                procedure.setParamters(parameterslist);
                //procedures.set(cnt, procedure
                proceduresList.add(procedure);
            }
            
            proceduresDefinition = proceduresDefinition.substring(proceduresDefinition.indexOf(";")+1, proceduresDefinition.length()).trim();
        }
        
        /*
         * sys_refcursor icin burda gelistirme yapilacak if i true ya cekip
         * procedure leri parse etmek gerek.
         */
        if(false)
        {
            for (Procedure procedure : proceduresList) {
                getProcedureScript(procedure.getProcedureName(), pkbScript);
            }
        }
        return proceduresList;
    }
    
    public List<Procedure> getProcedures2(Connection conn, String packageName, String pksScript)
    throws SQLException, IOException
    {
        List<Procedure> proceduresList = new ArrayList<Procedure>();
        //String proceduresDefinition = pksScript.substring(pksScript.indexOf("procedure"), pksScript.lastIndexOf("end")).trim();;
        //System.out.println("1: ");
        String oneProcedureDefinition = "";

        List<Parameter> parameterslist = getParameters2(conn, packageName, pksScript);
        
        
        for (Parameter parameter : parameterslist) {
            
        }
        
        return proceduresList;
    }
    
    public String getProcedureScript(String procedureName, String pkbScript)
    {
        String procedureString = "";
        String procedureStringTemp = "";
        //procedureStringTemp = pkbScript.substring(pkbScript.indexOf("end;"), 1000).trim();
        
        
        /*while(true)
        {
            procedureStringTemp = pkbScript.substring(pkbScript.indexOf("end"), 1000).trim();
            
            
            
        }*/
        Pattern pattern = Pattern.compile(".*end;");
        Matcher matcher = pattern.matcher(pkbScript);
        while(matcher.find()) {
            System.out.println(""+matcher.start());
            System.out.println(""+matcher.end());
            System.out.println(""+matcher.group());
        }
        //filtered = matcher.replaceAll(" ");
        
        return procedureString;
    }
    
    public String getProjectName()
    {
        return "WSGenerator";
    }
    
    public String validateProcedures(List<Procedure> procedures)
    {
        String returnValue = "OK";
        /*for (Procedure procedure : procedures) {
            for (Parameter parameter : procedure.getParamters()) {
                if("date".equals(parameter.getParameterType()))
                {
                    returnValue = "Invalid Parameter Type in "+procedure.getProcedureName()+ " procedure. Please change date type parameters to varchar2 in database package.";
                }
            }
        }*/
        
        return returnValue;
    }
}
