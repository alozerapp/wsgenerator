/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.template;

import antlr.CodeGenerator;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateErrorListener;
import org.antlr.stringtemplate.StringTemplateGroup;
import org.antlr.stringtemplate.language.AngleBracketTemplateLexer;
import org.antlr.stringtemplate.language.DefaultTemplateLexer;
import ws.file.FileUtils;
import ws.model.*;
        

/**
 *
 * @author EXT0126117
 */
public class TemplateUtils {
    
    public String getPropertiesContent(String userDB, String passwordDB, String hostDB, String portDB, String sidDB, String templatesDirectory)
    throws IOException
    {
        String content = "";
        
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        String templateContent = getTemplateContent(templatesDirectory, "properties.st");
        StringTemplate st = new StringTemplate(templateContent);
            
        //StringTemplate st = templateGroup.getInstanceOf("WebMethodContent"); 

        st.setAttribute("user_db", userDB);
        st.setAttribute("password_db", passwordDB);
        st.setAttribute("host_db", hostDB);
        st.setAttribute("port_db", portDB);
        st.setAttribute("sid_db", sidDB);

        content = content+st.toString();
        
        return content;
    }
    
    public String getHandlerChainContent(String templatesDirectory)
    {
        //FileUtils fileUtils = new FileUtils();
        System.out.println("templatesDirectory: "+templatesDirectory);

        String templateContent = getTemplateContent(templatesDirectory ,"HandlerChain.st");
            
        StringTemplate st = new StringTemplate(templateContent);

        return st.toString();
    }
    
    private InputStream readInputFile(String templateFile) {
        InputStream is = CodeGenerator.class.getClassLoader().getResourceAsStream(templateFile);
        if (null == is) {
            throw new RuntimeException("Template file does not exist: " + templateFile);
        }
        return is;
    }
    
    public String getTemplateContent(String templatesDirectory ,String templatename)
    {
        InputStream inputStream = (InputStream) readInputFile(templatesDirectory+templatename);
        StringBuilder builder = new StringBuilder();
        int ch;
        try {
            while((ch = inputStream.read()) != -1){
                builder.append((char)ch);
            }
        } catch (IOException ex) {
            System.out.println("eroor: "+ ex.toString());
        }
        
        return builder.toString();
    }
    
    public boolean isPagingExits (List<Parameter> parameters)
    {
        boolean isPagingExits = false;
        for (Parameter parameter : parameters) {
            if(getPagingType().equals(parameter.getParameterType().toLowerCase()))
            {
                isPagingExits = true;
            }
        }

        return isPagingExits;
    }
    
    public String getWebServiceContent(boolean appsHeaderFlag, String templatesDirectory, String webServiceName, String dataSourceName, List<Procedure> procedures, String requestPrefix, String responsePrefix, String listFileAnnotation)
    throws IOException
    {
        //WebServiceContent webServiceContent = new WebServiceContent();
        String webMethodsContent = "";
        //webServiceContent.setWebServiceName(webServiceName);
        //webServiceContent.setDataSourceName(dataSourceName);
        boolean isPagingExits = false;
        boolean appsHeaderFlagInt = appsHeaderFlag;
        for (Procedure procedure : procedures) {
            boolean appsHeaderPagingFlag = isPagingExits(procedure.getParamters());
            if(appsHeaderPagingFlag)
            {
                appsHeaderFlagInt = appsHeaderPagingFlag;
                isPagingExits = appsHeaderPagingFlag;
            }else
            {
                appsHeaderFlagInt = appsHeaderFlag;
            }
            //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
            //StringTemplateGroup sdsd = new StringTemplateGroup
            
            String templateContent = getTemplateContent(templatesDirectory ,"WebMethod.st");
            
            StringTemplate st = new StringTemplate(templateContent);

            st.setAttribute("web_method_name", procedure.getProcedureName().toUpperCase());
            st.setAttribute("request_class_name", procedure.getProcedureName().toUpperCase()+requestPrefix);
            st.setAttribute("response_class_name", procedure.getProcedureName().toUpperCase()+responsePrefix);
            if(appsHeaderFlagInt)
            {
                st.setAttribute("define_apps_header", getAppsHeader(templatesDirectory, appsHeaderPagingFlag));
            }
            st.setAttribute("web_method_content", getWebmethodContent(appsHeaderFlagInt, appsHeaderPagingFlag, templatesDirectory, webServiceName, procedure, responsePrefix, listFileAnnotation));
            if(appsHeaderFlagInt)
            {
                st.setAttribute("set_apps_header", getSetAppsHeader(templatesDirectory));
            }
            webMethodsContent = webMethodsContent+st.toString()+"\n\n";
        }
        
        //webServiceContent.setWebMethodsContent(webMethodsContent);
        
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //StringTemplate st = templateGroup.getInstanceOf("WebService"); 
        String templateContent = getTemplateContent(templatesDirectory ,"WebService.st");
        StringTemplate st = new StringTemplate(templateContent);
        st.setAttribute("imports", getAppsHeaderImports(templatesDirectory, isPagingExits, appsHeaderFlag));
        st.setAttribute("web_service_name", webServiceName);
        st.setAttribute("data_source_name", dataSourceName);
        st.setAttribute("web_methods_content", webMethodsContent);
        
        return st.toString();
    }
    
    public String getAppsHeader(String templatesDirectory, boolean appsHeaderPagingFlag)
    throws IOException
    {
        
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //String templateContent = getTemplateContent(templatesDirectory ,"WebMethod.st");
            
        //StringTemplate st = new StringTemplate(templateContent);
        StringTemplate st = null;
        
        if(appsHeaderPagingFlag)
        {
            //st = templateGroup.getInstanceOf("AppsHeaderPaging");
            st = new StringTemplate(getTemplateContent(templatesDirectory, "AppsHeaderPaging.st"));
        }else
        {
            //st = templateGroup.getInstanceOf("AppsHeader");
            st = new StringTemplate(getTemplateContent(templatesDirectory, "AppsHeader.st"));
        }
        
        return st.toString();
    }
    
    public String getAppsHeaderImports(String templatesDirectory, boolean appsHeaderPagingFlag, boolean appsHeaderFlag)
    throws IOException
    {
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //String templateContent = getTemplateContent(templatesDirectory ,"WebService.st");
        //StringTemplate st = new StringTemplate(templateContent);
        StringTemplate st = null;
        
        if(appsHeaderPagingFlag)
        {
            //st = templateGroup.getInstanceOf("AppsHeaderPagingImports");
            st = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderPagingImports.st"));
        }else if(appsHeaderFlag)
        {
            //st = templateGroup.getInstanceOf("AppsHeaderImports");
            st = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderImports.st"));
        }else
        {
            st = new StringTemplate("");
        }
        return st.toString();
    }
    
    public String getSetAppsHeader(String templatesDirectory)
    throws IOException
    {
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //StringTemplate st = templateGroup.getInstanceOf("SetAppsHeader");
        String templateContent = getTemplateContent(templatesDirectory ,"SetAppsHeader.st");
        StringTemplate st = new StringTemplate(templateContent);
        
        return st.toString();
    }
    
    public String getWebmethodContent(boolean appsHeaderFlag, boolean appsHeaderPagingFlag, String templatesDirectory, String webServiceName, Procedure procedure, String responsePrefix, String listFileAnnotation)
    throws IOException
    {
        String webmethodContent = "";
        
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        String templateContent = getTemplateContent(templatesDirectory ,"WebMethodContent.st");
        StringTemplate st = new StringTemplate(templateContent);
            
        //StringTemplate st = templateGroup.getInstanceOf("WebMethodContent"); 
        st.setAttribute("procedure_sql_call", getProcedureSqlCall(appsHeaderFlag, appsHeaderPagingFlag, templatesDirectory, webServiceName, procedure));
        st.setAttribute("response_class", procedure.getProcedureName().toUpperCase()+responsePrefix);
        st.setAttribute("set_response_out_parameters", getSetResponseOutParameters(templatesDirectory, webServiceName, procedure));
        st.setAttribute("set_list_values", getSetListValues(templatesDirectory, webServiceName, procedure, listFileAnnotation));

        webmethodContent = webmethodContent+st.toString();
        
        return webmethodContent;
    }
    
    public String getProcedureSqlCall(boolean appsHeaderFlag, boolean appsHeaderPagingFlag, String templatesDirectory, String webServiceName, Procedure procedure)
    {
        
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        String templateContent = getTemplateContent(templatesDirectory ,"ProcedureSqlCall.st");
            
        StringTemplate st = new StringTemplate(templateContent);
        //StringTemplate st = templateGroup.getInstanceOf("ProcedureSqlCall"); 
        
        st.setAttribute("map", getMapContent(procedure, templatesDirectory));
        
        if(appsHeaderFlag)
        {
            st.setAttribute("initialize_sql_call", getInitializeSql(templatesDirectory));
        }
        
        st.setAttribute("package_name", webServiceName);
        st.setAttribute("procedure_name", procedure.getProcedureName());
        st.setAttribute("question_marks", getQuestionMarks(procedure.getParamters().size()));
        st.setAttribute("set_parameters", getSetParameters(appsHeaderFlag, appsHeaderPagingFlag, procedure, templatesDirectory));
        
        return st.toString();
    }
    
    public String getMapContent(Procedure procedure, String templatesDirectory)
    {
        String mapContent = "";
        String mapRowsContent = "";
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        String templateContent = getTemplateContent(templatesDirectory ,"Map.st");
        StringTemplate st = new StringTemplate(templateContent);
        //StringTemplate st = templateGroup.getInstanceOf("Map"); 
        for (Parameter parameters : procedure.getParamters()) {
            if("Object".equals(parameters.getParameterTypeJava()))
            {
                if(!"xxtg_ef_mo_ws_paging".equals(parameters.getParameterType()))
                {
                    for (Type type : parameters.getTypes()) {
                        //for (TypeColumn typeColumn : type.getTypeColumns()) {
                        //StringTemplate st2 = templateGroup.getInstanceOf("MapRow"); 
                        if(type.isCustomType())
                        {
                            StringTemplate st2 = new StringTemplate(getTemplateContent(templatesDirectory ,"MapRow.st"));
                            st2.setAttribute("type_name", type.getTypeName().toUpperCase());
                            mapRowsContent = mapRowsContent + st2.toString()+"\n";
                        }
                        //}
                    }
                    
                }
            }
        }
        st.setAttribute("map_rows", mapRowsContent);
        mapContent = st.toString()+"\n";
        
        return mapContent;
    }
    
    public String getInitializeSql(String templatesDirectory)
    {
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //StringTemplate st = templateGroup.getInstanceOf("InitializeSqlCall"); 
        String templateContent = getTemplateContent(templatesDirectory ,"InitializeSqlCall.st");  
        StringTemplate st = new StringTemplate(templateContent);
        
        return st.toString();
    }
    
    public String getSetResponseOutParameters(String templatesDirectory, String webServiceName, Procedure procedure)
    {
        String setResponseOutParameters = "";
        int index = 1;
        for (Parameter parameter : procedure.getParamters()) {
            if("blob".equals(parameter.getParameterType()))
            {
                String test = "";
            }
            if("out".equals(parameter.getParameterDirection()))
            {
                if("sys_refcursor".equals(parameter.getParameterType())) 
                {
                    //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
                    //StringTemplate st = templateGroup.getInstanceOf("ResponseOutParameterSysRefcursor");
                    String templateContent = getTemplateContent(templatesDirectory ,"ResponseOutParameterSysRefcursor.st");  
                    StringTemplate st = new StringTemplate(templateContent);
                    st.setAttribute("list_number", index);
                    st.setAttribute("parameter_name", parameter.getParameterName());
                    setResponseOutParameters = setResponseOutParameters + st.toString()+"\n";
               }else if("refcursor".equals(parameter.getParameterTypeJava())) 
               {
                    //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
                    //StringTemplate st = templateGroup.getInstanceOf("ResponseOutParameterSysRefcursor");
                    String templateContent = getTemplateContent(templatesDirectory ,"ResponseOutParameterSysRefcursor.st");  
                    StringTemplate st = new StringTemplate(templateContent);
                    st.setAttribute("list_number", index);
                    st.setAttribute("parameter_name", parameter.getParameterName());
                    setResponseOutParameters = setResponseOutParameters + st.toString()+"\n";
               }else if("Object".equals(parameter.getParameterTypeJava()))
               {
                    //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
                    //StringTemplate st = templateGroup.getInstanceOf("ResponseOutParameterType");
                    String templateContent = getTemplateContent(templatesDirectory ,"ResponseOutParameterType.st");  
                    StringTemplate st = new StringTemplate(templateContent);
                    st.setAttribute("set_type_value", getSetTypeValue(templatesDirectory, parameter, index));
                    st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());
                    //st.setAttribute("parameter_type_java", parameter.getParameterTypeJava());
                    st.setAttribute("parameter_name", parameter.getParameterNameJavaMethod().toLowerCase());
                    setResponseOutParameters = setResponseOutParameters + st.toString()+"\n\n";
               }else if("blob".equals(parameter.getParameterType())) 
               {
                    //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
                    //StringTemplate st = templateGroup.getInstanceOf("ResponseOutParameter");
                    String templateContent = getTemplateContent(templatesDirectory ,"ResponseOutParameter.st");  
                    StringTemplate st = new StringTemplate(templateContent);
                    st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());
                    st.setAttribute("parameter_type_java", parameter.getParameterTypeJavaMethod());
                    st.setAttribute("parameter_name", parameter.getParameterName());
                    setResponseOutParameters = setResponseOutParameters + st.toString()+"\n";
               }else
               {
                    //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
                    //StringTemplate st = templateGroup.getInstanceOf("ResponseOutParameter");
                    String templateContent = getTemplateContent(templatesDirectory ,"ResponseOutParameter.st");  
                    StringTemplate st = new StringTemplate(templateContent);
                    st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());
                    st.setAttribute("parameter_type_java", parameter.getParameterTypeJava());
                    st.setAttribute("parameter_name", parameter.getParameterName());
                    setResponseOutParameters = setResponseOutParameters + st.toString()+"\n";
               }
               index++;
            }
        }
        
        return setResponseOutParameters;
    }
    
    public String getSetTypeValue(String templatesDirectory, Parameter parameter, int index)
    {
        String setTypeValue = "";
        
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
        //StringTemplate st = templateGroup.getInstanceOf("SetTypeValue");
        String templateContent = null;
        StringTemplate st = null;
        if("RECORD".equals(getParameterTypeClass(parameter)))
        {
            templateContent = getTemplateContent(templatesDirectory ,"SetTypeValueRecord.st");  
            st = new StringTemplate(templateContent);
            st.setAttribute("cs_parameter_name", parameter.getParameterName());
            st.setAttribute("parameter_name", parameter.getParameterNameJavaMethod().toLowerCase());
            st.setAttribute("parameter_type", parameter.getParameterType().toUpperCase());
        }else
        {
            templateContent = getTemplateContent(templatesDirectory ,"SetTypeValueTable.st");  
            st = new StringTemplate(templateContent);
            st.setAttribute("cs_parameter_name", parameter.getParameterName());
            st.setAttribute("parameter_name", parameter.getParameterNameJavaMethod().toLowerCase());
            st.setAttribute("parameter_type", parameter.getParameterType().toUpperCase());
            st.setAttribute("index", index);
        }
        
        setTypeValue = setTypeValue + st.toString();
        
        return setTypeValue;
    }
    
    public String getParameterTypeClass(Parameter parameter)
    {
        String typeClass = "";
        if(parameter.getTypes() != null)
        {
            for (Type type : parameter.getTypes()) {
                if(parameter.getParameterType().equals(type.getTypeName()))
                {
                    typeClass = type.getTypeClass();
                }
            }
        }
        return typeClass;
    }
    
    public String getSetListValues(String templatesDirectory, String webServiceName, Procedure procedure, String listFileAnnotation)
    throws IOException
    {
        FileUtils fileUtils = new FileUtils();
        String setListValues = "";
        int listNumber = 1;
        for (Parameter parameter : procedure.getParamters()) {
            if("out".equals(parameter.getParameterDirection()))
            {
                    if("sys_refcursor".equals(parameter.getParameterType()))
                    {
                        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
                        //StringTemplate st = templateGroup.getInstanceOf("ListValues");
                        String templateContent = getTemplateContent(templatesDirectory ,"ListValues.st");  
                        StringTemplate st = new StringTemplate(templateContent);
                        st.setAttribute("parameter_name", parameter.getParameterNameJava());
                        st.setAttribute("list_number", listNumber);
                        st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());
                        st.setAttribute("list_param_class_name", parameter.getParameterNameJava()+"_"+listFileAnnotation);
                        st.setAttribute("list_param_name", parameter.getParameterNameJava().toLowerCase()+"_"+listFileAnnotation.toLowerCase());
                        st.setAttribute("list_param_name_camel_case", parameter.getParameterNameJavaMethod()+"_"+listFileAnnotation.toLowerCase());
                        setListValues = setListValues + st.toString()+"\n";
                        fileUtils.createListFiles(templatesDirectory, parameter, webServiceName);
                        listNumber++;
                    }else if("refcursor".equals(parameter.getParameterTypeJava()))
                    {
                        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
                        //StringTemplate st = templateGroup.getInstanceOf("ListValues");
                        String templateContent = getTemplateContent(templatesDirectory ,"ListValues.st");  
                        StringTemplate st = new StringTemplate(templateContent);
                        st.setAttribute("parameter_name", parameter.getParameterNameJava());
                        st.setAttribute("list_number", listNumber);
                        st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());
                        st.setAttribute("list_param_class_name", parameter.getParameterNameJava()+"_"+listFileAnnotation);
                        st.setAttribute("list_param_name", parameter.getParameterNameJava().toLowerCase()+"_"+listFileAnnotation.toLowerCase());
                        st.setAttribute("list_param_name_camel_case", parameter.getParameterNameJavaMethod()+"_"+listFileAnnotation.toLowerCase());
                        setListValues = setListValues + st.toString()+"\n";
                        fileUtils.createListFiles(templatesDirectory, parameter, webServiceName);
                        listNumber++;
                    }else if("type".equals(parameter.getParameterType()))
                    {
                        ;  
                    }else
                    {
                        ;
                    }
                
            }
        }
        
        return setListValues;
    }
    
    
    public String getListLineContent(String templatesDirectory, String paramterName)
    throws IOException
    {
        String listContent = "";
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
        //StringTemplate st = templateGroup.getInstanceOf("ListLine");
        String templateContent = getTemplateContent(templatesDirectory ,"ListLine.st");  
        StringTemplate st = new StringTemplate(templateContent);
        st.setAttribute("class_name", paramterName);
        listContent = listContent + st.toString()+"\n";
        
        return listContent;
    }
    
    public String getListContent(String templatesDirectory, Parameter parameter)
    throws IOException
    {
        String listContent = "";
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
        //StringTemplate st = templateGroup.getInstanceOf("List");
        String templateContent = getTemplateContent(templatesDirectory ,"List.st");  
        StringTemplate st = new StringTemplate(templateContent);
        st.setAttribute("class_name", parameter.getParameterNameJava()+"_LIST");
        st.setAttribute("parameter_name_class", parameter.getParameterNameJava());
        st.setAttribute("parameter_name", parameter.getParameterNameJava().toLowerCase());
        st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());
        listContent = listContent + st.toString()+"\n";
        
        return listContent;
    }
    
    public String getTableTypeContentArrayContent(String templatesDirectory, String variableName)
    throws IOException
    {
        String listContent = "";
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
        //StringTemplate st = templateGroup.getInstanceOf("List");
        String templateContent = getTemplateContent(templatesDirectory ,"TableTypeContentArray.st");  
        StringTemplate st = new StringTemplate(templateContent);
        st.setAttribute("variable_name", variableName);
        
        listContent = listContent + st.toString()+"\n";
        
        return listContent;
    }
    
    public String getTableTypeContentStructContent(String templatesDirectory, String variableName)
    throws IOException
    {
        String listContent = "";
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
        //StringTemplate st = templateGroup.getInstanceOf("List");
        String templateContent = getTemplateContent(templatesDirectory ,"TableTypeContentStruct.st");  
        StringTemplate st = new StringTemplate(templateContent);
        st.setAttribute("variable_name", variableName);
        
        listContent = listContent + st.toString()+"\n";
        
        return listContent;
    }
    
    public String getTypeFileContent(String templatesDirectory, String packageName, Type type)
    throws IOException
    {
        String typeFileContent = "";
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
        StringTemplate st;
        if (type.getTypeClass().equals("RECORD")) {
                //st = templateGroup.getInstanceOf("RecordType");
                st = new StringTemplate(getTemplateContent(templatesDirectory ,"RecordType.st"));
                st.setAttribute("class_name", type.getTypeName().toUpperCase());
                st.setAttribute("variables", getVariables(templatesDirectory, packageName, type));
                st.setAttribute("read_sql", getReadSql(templatesDirectory, packageName, type));
                st.setAttribute("struct", getStruct(templatesDirectory, packageName, type));
                st.setAttribute("get_set_methods", getGetSetMethods(templatesDirectory, packageName, type));
                typeFileContent = typeFileContent + st.toString()+"\n";
        }else if (type.getTypeClass().equals("TABLE")) {
                for (TypeColumn typeColumn : type.getTypeColumns()) {
                    //st = templateGroup.getInstanceOf("TableType");
                    st = new StringTemplate(getTemplateContent(templatesDirectory ,"TableType.st"));
                    st.setAttribute("class_name", type.getTypeName().toUpperCase());
                    if(typeColumn.isCustomType())
                    {
                        st.setAttribute("variable_type", typeColumn.getColumnType().toUpperCase());
                    }else
                    {
                        st.setAttribute("variable_type", typeColumn.getColumnTypeJava());
                    }
                    
                    st.setAttribute("variable_name", typeColumn.getColumnNameJava().toLowerCase());
                    st.setAttribute("variable_method", typeColumn.getColumnNameJavaMethod());
                    String arrayStructContent = "";
                    if(typeColumn.isCustomType())
                    {
                        arrayStructContent = getTableTypeContentStructContent(templatesDirectory,typeColumn.getColumnNameJava().toLowerCase());
                    }else
                    {
                        arrayStructContent = getTableTypeContentArrayContent(templatesDirectory,typeColumn.getColumnNameJava().toLowerCase());
                    }
                    st.setAttribute("array_struct_content", arrayStructContent);

                    typeFileContent = typeFileContent + st.toString()+"\n";
                }
        }
        
        return typeFileContent;
    }
    
    public String getVariables(String templatesDirectory, String packageName, Type type)
    throws IOException
    {
        String variables = "";
        
        for (TypeColumn typeColumn : type.getTypeColumns()) {
            //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
            //StringTemplate st = templateGroup.getInstanceOf("ParamDefiniton"); 
            String templateContent = getTemplateContent(templatesDirectory ,"ParamDefiniton.st");  
            StringTemplate st = new StringTemplate(templateContent);
            st.setAttribute("param_access_modifier", "private");
            if("Object".equals(typeColumn.getColumnTypeJava()))
            {
                st.setAttribute("param_type", typeColumn.getColumnType().toUpperCase());
            }else
            {
                st.setAttribute("param_type", typeColumn.getColumnTypeJava());
            }
            st.setAttribute("param_name", typeColumn.getColumnNameJava().toLowerCase());

            variables = variables+st.toString()+"\n";
        }
        
        return variables;
    }
    
    
    
    public String getReadSql(String templatesDirectory, String packageName, Type type)
    throws IOException
    {
        String readSql = "";
        for (TypeColumn typeColumn : type.getTypeColumns()) {
            //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
            StringTemplate st = null;
            //String templateContent = getTemplateContent(templatesDirectory ,"ParamDefiniton.st");  
            //StringTemplate st = new StringTemplate(templateContent);
            if("Object".equals(typeColumn.getColumnTypeJava()))
            {
                readSql = readSql+"\n";
                //st = templateGroup.getInstanceOf("ReadSqlTypeRow"); 
                st = new StringTemplate(getTemplateContent(templatesDirectory ,"ReadSqlTypeRow.st"));
                st.setAttribute("param_name", typeColumn.getColumnNameJava().toLowerCase());
                //st.setAttribute("param_type", typeColumn.getColumnType().toUpperCase());
                st.setAttribute("param_name_camel_case", typeColumn.getColumnNameJavaMethod());
                if(typeColumn.getColumnTypeClass().equals("TABLE"))
                {
                    st.setAttribute("variable_name", "array");
                    st.setAttribute("read_type", "readArray");
                    st.setAttribute("param_type_field", "new "+typeColumn.getColumnType().toUpperCase()+"(array)");
                }else if(typeColumn.getColumnTypeClass().equals("RECORD"))
                {
                    st.setAttribute("variable_name", "object");
                    st.setAttribute("read_type", "readObject");
                    st.setAttribute("param_type_field", "("+typeColumn.getColumnType().toUpperCase()+")object");
                }
            }else
            {
                //st = templateGroup.getInstanceOf("ReadSqlRow"); 
                st = new StringTemplate(getTemplateContent(templatesDirectory ,"ReadSqlRow.st"));
                st.setAttribute("name_method", typeColumn.getColumnNameJavaMethod());
                st.setAttribute("type_method", typeColumn.getColumnTypeJavaMethod());
            }                

            readSql = readSql+st.toString()+"\n";
        }
        return readSql;
    }
    
    public String getStruct(String templatesDirectory, String packageName, Type type)
    throws IOException
    {
        String struct = "";
        String structRows = "";
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
        //StringTemplate st = templateGroup.getInstanceOf("Struct");
        String templateContent = getTemplateContent(templatesDirectory ,"Struct.st");  
        StringTemplate st = new StringTemplate(templateContent);
        st.setAttribute("struct_size", type.getTypeColumns().size());
        int index = 0;
        
        for (TypeColumn typeColumn : type.getTypeColumns()) {
            StringTemplate st2 = null;
            if("Object".equals(typeColumn.getColumnTypeJava()))
            {
                structRows = structRows+"\n";
                //st2 = templateGroup.getInstanceOf("StructTypeRow");
                st2 = new StringTemplate( getTemplateContent(templatesDirectory ,"StructTypeRow.st"));
                st2.setAttribute("index", index);
                st2.setAttribute("param_name", typeColumn.getColumnNameJava().toLowerCase());
                if(typeColumn.getColumnTypeClass().equals("RECORD"))
                {
                    st2.setAttribute("create_method", "createStruct");
                }else if(typeColumn.getColumnTypeClass().equals("TABLE"))
                {
                    st2.setAttribute("create_method", "createArray");
                }
            }else if("byte[]".equals(typeColumn.getColumnTypeJava()))
            {
                st2 = new StringTemplate( getTemplateContent(templatesDirectory ,"StructRowBlob.st"));
                st2.setAttribute("index", index);
                st2.setAttribute("name_method", typeColumn.getColumnNameJavaMethod());
            }else if("Date".equals(typeColumn.getColumnTypeJava()))
            {
                st2 = new StringTemplate( getTemplateContent(templatesDirectory ,"StructRow.st"));
                st2.setAttribute("index", index);
                st2.setAttribute("name_method", typeColumn.getColumnNameJavaMethod());
                st2.setAttribute("name_method_before", "new java.sql.Date(");
                st2.setAttribute("name_method_after", ".getTime())");
            }else
            {
                //st2 = templateGroup.getInstanceOf("StructRow");
                st2 = new StringTemplate( getTemplateContent(templatesDirectory ,"StructRow.st"));
                st2.setAttribute("index", index);
                st2.setAttribute("name_method", typeColumn.getColumnNameJavaMethod());
            }

            structRows = structRows + st2.toString()+"\n";
            index++;
        }
        
        st.setAttribute("struct_rows", structRows);
        struct = st.toString();
        
        return struct;
    }
    
    public String getGetSetMethods(String templatesDirectory, String packageName, Type type)
    throws IOException
    {
        String getSetMethods = "";
        for (TypeColumn typeColumn : type.getTypeColumns()) {
            //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory);
            //StringTemplate st = templateGroup.getInstanceOf("GetSet");
            String templateContent = getTemplateContent(templatesDirectory ,"GetSet.st");  
            StringTemplate st = new StringTemplate(templateContent);
            if("Object".equals(typeColumn.getColumnTypeJava()))
            {
                st.setAttribute("param_type", typeColumn.getColumnType().toUpperCase());
            }else
            {
                st.setAttribute("param_type", typeColumn.getColumnTypeJava());
            }
            st.setAttribute("param_name", typeColumn.getColumnNameJava().toLowerCase());
            st.setAttribute("param_name_camel_case", typeColumn.getColumnNameJavaMethod());
            getSetMethods = getSetMethods + st.toString()+"\n";
        }
        
        return getSetMethods;
    }
    
    
    public String getQuestionMarks(int cnt)
    {
        String questionMarks = "";
        if (cnt > 0) {
            questionMarks = questionMarks + "(";
        }
        
        for (int i = 0; i < cnt; i++) {
            questionMarks = questionMarks + "?,";
        }
        //System.out.println(questionMarks.substring(questionMarks.length()-2, questionMarks.length()));
        if (",".equals(questionMarks.substring(questionMarks.length()-1, questionMarks.length())))
        {
            questionMarks = questionMarks.substring(0 ,questionMarks.length()-1);
        }
        
        if (cnt > 0) {
            questionMarks = questionMarks + ")";
        }
        
        return questionMarks;
    }
    
    public String getSetParameters(boolean appsHeaderFlag, boolean appsHeaderPagingFlag, Procedure procedure, String templatesDirectory)
    {
        String setParameters = "";
        if(appsHeaderFlag)
        {
            setParameters = setParameters + getinitializeSetParam(templatesDirectory);
            
            /*if(appsHeaderPagingFlag)
            {
                    setParameters = setParameters + getSetPatameterPaging(templatesDirectory);
            }*/
        }
        
        
        for (Parameter parameter : procedure.getParamters()) 
        {
            if(getPagingType().equals(parameter.getParameterType().toLowerCase()) && appsHeaderPagingFlag)
            {
                setParameters = setParameters + getSetPatameterPaging(templatesDirectory);
            }else
            {
                setParameters = setParameters + getSetPatameter(appsHeaderFlag, appsHeaderPagingFlag, parameter, templatesDirectory)+"\n";
            }
        }
        
        return setParameters;
    }
    
    public String getinitializeSetParam(String templatesDirectory)
    {
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //StringTemplate st = templateGroup.getInstanceOf("InitializeSetParam");
        String templateContent = getTemplateContent(templatesDirectory ,"InitializeSetParam.st");  
        StringTemplate st = new StringTemplate(templateContent);
        
        return st.toString()+"\n";
    }
    
    public String getSetPatameterPaging(String templatesDirectory)
    {
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //StringTemplate st = templateGroup.getInstanceOf("SetInParameterPaging");
        String templateContent = getTemplateContent(templatesDirectory ,"SetInParameterPaging.st");  
        StringTemplate st = new StringTemplate(templateContent);
        
        return st.toString()+"\n";
    }
    
    public String getSetPatameterType(String templatesDirectory, Parameter parameter)
    {
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //StringTemplate st = templateGroup.getInstanceOf("SetInParameterType");
        String templateContent = "";
        StringTemplate st = null;
        if("RECORD".equals(parameter.getTypes().get(0).getTypeClass()))
        {
            templateContent = getTemplateContent(templatesDirectory ,"SetInParameterType.st");
            st = new StringTemplate(templateContent);
            st.setAttribute("parameter_name", parameter.getParameterName());
            st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());
            st.setAttribute("create_method_name", "Struct");
        }else if("TABLE".equals(parameter.getTypes().get(0).getTypeClass()))
        {
            templateContent = getTemplateContent(templatesDirectory ,"SetInParameterTableType.st");
            st = new StringTemplate(templateContent);
            st.setAttribute("parameter_name", parameter.getParameterName());
            st.setAttribute("parameter_name_lower", parameter.getParameterName().toLowerCase());
            st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());
            st.setAttribute("parameter_type_upper", parameter.getParameterType().toUpperCase());
            st.setAttribute("create_method_name", "Array");
        }
        
        return st.toString()+"\n";
    }
    
    public String getSetPatameter(boolean appsHeaderFlag, boolean appsHeaderPagingFlag, Parameter parameter, String templatesDirectory)
    {
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        StringTemplate st = null;
        //String templateContent = getTemplateContent(templatesDirectory ,"SetInParameterType.st");  
        //StringTemplate st = new StringTemplate(templateContent);
        String getSetParameter = "";
        String oracleType = getOracleType(parameter);
        if (parameter.getParameterDirection().equals("in")) {
                //st = templateGroup.getInstanceOf("SetInParameter");
                st = new StringTemplate(getTemplateContent(templatesDirectory ,"SetInParameter.st"));
                if("Object".equals(parameter.getParameterTypeJava()))
                {
                    getSetParameter = getSetPatameterType(templatesDirectory, parameter);
                }else
                {
                    if("blob".equals(parameter.getParameterType()))
                    {
                        st.setAttribute("parameter_type_java", parameter.getParameterTypeJavaMethod());
                    }else
                    {
                        st.setAttribute("parameter_type_java", parameter.getParameterTypeJava());
                    }
                    
                    if("date".equals(parameter.getParameterType()))
                    {
                        st.setAttribute("param_name_camel_case_before", "new java.sql.Date(");
                        st.setAttribute("param_name_camel_case_after", ".getTime())");
                    }

                    st.setAttribute("parameter_name", parameter.getParameterName());
                    st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());
                    
                    
                    
                    getSetParameter = st.toString();
                }
        }else if (parameter.getParameterDirection().equals("out"))
        {
                
                //st = templateGroup.getInstanceOf("SetOutParameter");
                st = new StringTemplate(getTemplateContent(templatesDirectory ,"SetOutParameter.st"));
                st.setAttribute("parameter_name", parameter.getParameterName());
                st.setAttribute("oracle_type_name", oracleType);
                if("ARRAY".equals(oracleType))
                {
                    st.setAttribute("type_name", ", \"APPS."+parameter.getParameterType().toUpperCase()+"\"");
                }else if("STRUCT".equals(oracleType))
                {
                    st.setAttribute("type_name", ", \"APPS."+parameter.getParameterType().toUpperCase()+"\"");
                }
                
                getSetParameter = st.toString();
        }
        
        return getSetParameter;
    }
    
    public String getOracleType(Parameter parameter)
    {
        String oracleType = "";
        
        if("number".equals(parameter.getParameterType())) 
        {
            oracleType = "NUMBER";
        }else if("varchar2".equals(parameter.getParameterType())) 
        {
            oracleType = "VARCHAR";
        }else if("sys_refcursor".equals(parameter.getParameterType())) 
        {
            oracleType = "CURSOR";
        }else if("refcursor".equals(parameter.getParameterTypeJava())) 
        {
            oracleType = "CURSOR";
        }else if("clob".equals(parameter.getParameterType())) 
        {
            oracleType = "CLOB";
        }else if("blob".equals(parameter.getParameterType())) 
        {
            oracleType = "BLOB";
        }else if("date".equals(parameter.getParameterType())) 
        {
            oracleType = "DATE";
        }else if("TABLE".equals(getParameterTypeClass(parameter))) 
        {
            oracleType = "ARRAY";
        }else if("RECORD".equals(getParameterTypeClass(parameter))) 
        {
            oracleType = "STRUCT";
        }else
        {
            oracleType = "";
        }
        
        return oracleType;
    }
    
    /*
     * generate web method request class
     */
    public String getRequestFileContent(boolean appsHeaderFlag, String templatesDirectory, String requestClassName, List<Parameter> parameters)
    {
        String paramDefinitionsReq = "";
        String paramSetterGetterReq = "";
        
        boolean appsHeaderPagingFlag = isPagingExits(parameters);
        if(appsHeaderPagingFlag)
        {
            appsHeaderFlag = appsHeaderPagingFlag;
        }
        
        for (Parameter parameter : parameters) {
            if ("in".equals(parameter.getParameterDirection()))
            {
                if(getPagingType().equals(parameter.getParameterType().toLowerCase()))
                {
                    ;
                }else
                {
                    //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                    //StringTemplate st = templateGroup.getInstanceOf("ParamDefiniton"); 
                    String templateContent = getTemplateContent(templatesDirectory ,"ParamDefiniton.st");  
                    StringTemplate st = new StringTemplate(templateContent);
                    st.setAttribute("param_access_modifier", "private");
                    if("Object".equals(parameter.getParameterTypeJava()))
                    {
                        st.setAttribute("param_type", parameter.getParameterType().toUpperCase());
                    }else
                    {
                        st.setAttribute("param_type", parameter.getParameterTypeJava());
                    }
                    st.setAttribute("param_name", parameter.getParameterNameJava().toLowerCase());

                    paramDefinitionsReq = paramDefinitionsReq+st.toString()+"\n";

                    //templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                    //st = templateGroup.getInstanceOf("GetSet");
                    st = new StringTemplate(getTemplateContent(templatesDirectory ,"GetSet.st"));
                    if("Object".equals(parameter.getParameterTypeJava()))
                    {
                        st.setAttribute("param_type", parameter.getParameterType().toUpperCase());
                    }else
                    {
                        st.setAttribute("param_type", parameter.getParameterTypeJava());
                    }
                    st.setAttribute("param_name", parameter.getParameterNameJava().toLowerCase());
                    st.setAttribute("param_name_camel_case", parameter.getParameterNameJavaMethod());

                    paramSetterGetterReq = paramSetterGetterReq+st.toString();
                }
            }
        }
        
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //StringTemplate st = templateGroup.getInstanceOf("Request"); 
        String templateContent = getTemplateContent(templatesDirectory ,"Request.st");  
        StringTemplate st = new StringTemplate(templateContent);
        
        st.setAttribute("class_name", requestClassName);
        st.setAttribute("param_definitions", paramDefinitionsReq);
        st.setAttribute("param_setter_getter", paramSetterGetterReq);
        
        if(appsHeaderFlag)
        {
            //StringTemplateGroup templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
            StringTemplate stInt = null;
            if(appsHeaderPagingFlag)
            {
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderRequestPagingImports");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderRequestPagingImports.st"));
                
                st.setAttribute("apps_header_request_imports", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderRequestPagingParamDefinitions");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderRequestPagingParamDefinitions.st"));
                
                st.setAttribute("apps_header_request_param_definitions", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderRequestPagingGetSet");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderRequestPagingGetSet.st"));
                
                st.setAttribute("apps_header_request_get_set", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderRequestPagingImplements");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderRequestPagingImplements.st"));
                
                st.setAttribute("apps_header_request_implemets", stInt.toString());
            }else
            {
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderRequestImports");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderRequestImports.st"));
                
                st.setAttribute("apps_header_request_imports", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderRequestParamDefinitions");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderRequestParamDefinitions.st"));
                
                st.setAttribute("apps_header_request_param_definitions", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderRequestGetSet");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderRequestGetSet.st"));
                
                st.setAttribute("apps_header_request_get_set", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderRequestImplements");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderRequestImplements.st"));
                
                st.setAttribute("apps_header_request_implemets", stInt.toString());
            }
        }
        
        return st.toString();
    }
    
    /*
     * generate web method respose class
     */
    public String getResponseFileContent(boolean appsHeaderFlag, String templatesDirectory, String responseClassName, List<Parameter> parameters, String listFileAnnotation)
    {   
        String paramDefinitionsRes = "";
        String paramSetterGetterRes = "";
        
        boolean appsHeaderPagingFlag = isPagingExits(parameters);
        if(appsHeaderPagingFlag)
        {
            appsHeaderFlag = appsHeaderPagingFlag;
        }
        
        for (Parameter parameter : parameters) {
            if ("out".equals(parameter.getParameterDirection()))
            {
                String paramType = "";
                String paramName = "";
                String param_NameCc = "";
                
                if (parameter.getParameterTypeJava().equals("sys_refcursor")) {
                        paramType = parameter.getParameterNameJava()+"_"+listFileAnnotation;
                        paramName = parameter.getParameterNameJava().toLowerCase()+"_"+listFileAnnotation.toLowerCase();
                        param_NameCc = parameter.getParameterNameJavaMethod()+"_"+listFileAnnotation.toLowerCase();
                }else if (parameter.getParameterTypeJava().equals("refcursor")){
                        paramType = parameter.getParameterNameJava()+"_"+listFileAnnotation;
                        paramName = parameter.getParameterNameJava().toLowerCase()+"_"+listFileAnnotation.toLowerCase();
                        param_NameCc = parameter.getParameterNameJavaMethod()+"_"+listFileAnnotation.toLowerCase();
                }else if (parameter.getParameterTypeJava().equals("Object")){
                        paramType = parameter.getParameterType().toUpperCase();
                        paramName = parameter.getParameterNameJava().toLowerCase();
                        param_NameCc = parameter.getParameterNameJavaMethod();
                }else{
                        paramType = parameter.getParameterTypeJava(); 
                        paramName = parameter.getParameterNameJava().toLowerCase();
                        param_NameCc = parameter.getParameterNameJavaMethod();
                }
                
                //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                StringTemplate st = null;
                if (parameter.getParameterTypeJava().equals("sys_refcursor")) {
                        //st = templateGroup.getInstanceOf("ParamDefiniton");
                        st = new StringTemplate(getTemplateContent(templatesDirectory ,"ParamDefiniton.st"));
                }if (parameter.getParameterTypeJava().equals("refcursor")){
                        //st = templateGroup.getInstanceOf("ParamDefiniton");
                        st = new StringTemplate(getTemplateContent(templatesDirectory ,"ParamDefiniton.st"));
                }if (parameter.getParameterTypeJava().equals("type")){
                        //st = templateGroup.getInstanceOf("ParamDefiniton");
                        st = new StringTemplate(getTemplateContent(templatesDirectory ,"ParamDefiniton.st"));
                }else{
                        //st = templateGroup.getInstanceOf("ParamDefiniton"); 
                        st = new StringTemplate(getTemplateContent(templatesDirectory ,"ParamDefiniton.st"));
                }       
                
                st.setAttribute("param_access_modifier", "private");
                st.setAttribute("param_type", paramType);
                st.setAttribute("param_name", paramName);
        
                paramDefinitionsRes = paramDefinitionsRes+st.toString()+"\n";
                
                //templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                if (parameter.getParameterTypeJava().equals("sys_refcursor")) {
                        //st = templateGroup.getInstanceOf("GetSet");
                        st = new StringTemplate(getTemplateContent(templatesDirectory ,"GetSet.st"));
                }else if(parameter.getParameterTypeJava().equals("refcursor")){
                        //st = templateGroup.getInstanceOf("GetSet");
                        st = new StringTemplate(getTemplateContent(templatesDirectory ,"GetSet.st"));
                }else if(parameter.getParameterTypeJava().equals("type")){
                        //st = templateGroup.getInstanceOf("GetSet");
                        st = new StringTemplate(getTemplateContent(templatesDirectory ,"GetSet.st"));
                }else{
                        //st = templateGroup.getInstanceOf("GetSet"); 
                        st = new StringTemplate(getTemplateContent(templatesDirectory ,"GetSet.st"));
                }
                
                st.setAttribute("param_type", paramType);
                st.setAttribute("param_name", paramName);
                st.setAttribute("param_name_camel_case", param_NameCc);
                
                paramSetterGetterRes = paramSetterGetterRes+st.toString()+"\n";
            }
        }
        
        //StringTemplateGroup templateGroup = new StringTemplateGroup("WSGenerator", templatesDirectory); 
        //StringTemplate st = templateGroup.getInstanceOf("Response");
        String templateContent = getTemplateContent(templatesDirectory ,"Response.st");  
        StringTemplate st = new StringTemplate(templateContent);
        st.setAttribute("class_name", responseClassName);
        st.setAttribute("param_definitions", paramDefinitionsRes);
        st.setAttribute("param_setter_getter", paramSetterGetterRes);
        
        if(appsHeaderFlag)
        {
            //StringTemplateGroup templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
            StringTemplate stInt = null;
            if(appsHeaderPagingFlag)
            {
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderResponsePagingImports");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderResponsePagingImports.st"));
            
                st.setAttribute("apps_header_response_imports", stInt.toString());
            
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderResponsePagingParamDefinitions");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderResponsePagingParamDefinitions.st"));
                
                st.setAttribute("apps_header_response_param_definitions", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderResponsePagingGetSet");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderResponsePagingGetSet.st"));
                
                st.setAttribute("apps_header_response_get_set", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderResponsePagingImplements");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderResponsePagingImplements.st"));
                
                st.setAttribute("apps_header_response_implemets", stInt.toString());
            }else
            {
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderResponseImports");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderResponseImports.st"));
                
                st.setAttribute("apps_header_response_imports", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderResponseParamDefinitions");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderResponseParamDefinitions.st"));
                
                st.setAttribute("apps_header_response_param_definitions", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderResponseGetSet");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderResponseGetSet.st"));
                
                st.setAttribute("apps_header_response_get_set", stInt.toString());
                
                //templateGroupInt = new StringTemplateGroup("WSGenerator", templatesDirectory); 
                //stInt = templateGroupInt.getInstanceOf("AppsHeaderResponseImplements");
                stInt = new StringTemplate(getTemplateContent(templatesDirectory ,"AppsHeaderResponseImplements.st"));
                
                st.setAttribute("apps_header_response_implemets", stInt.toString());
            }
        }
                
        return st.toString();
    }
    
    public String getPagingType()
    {
        return "xxtg_ef_mo_ws_paging";
    }
}
