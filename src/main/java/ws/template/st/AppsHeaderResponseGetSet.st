@Override
public ResponseHeader getResponseHeader() {
	return responseHeader;
}

/**
 * @param responseHeader the responseHeader to set
 */
@Override
public void setResponseHeader(ResponseHeader responseHeader) {
	this.responseHeader = responseHeader;
}