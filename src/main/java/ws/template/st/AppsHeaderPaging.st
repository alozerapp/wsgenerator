AppsHeader appsHeader = new AppsHeader(); 
appsHeader.setUserName(appsHeader, request.getRequestHeader().getUserIdentity().getUserName());
appsHeader.setNLSLanguage(request.getRequestHeader().getLanguage());
appsHeader.setOrg_Id(request.getRequestHeader().getOrg_id());
appsHeader.setRespApplication(request.getRequestHeader().getResp_application());
appsHeader.setResponsibility(request.getRequestHeader().getRespKey());
appsHeader.setSecurityGroup(request.getRequestHeader().getSecurity_group());

Paging pagingReq = new Paging();
pagingReq.setPageNumber(request.getPaging().getPageNumber());
pagingReq.setPageSize(request.getPaging().getPageSize());
pagingReq.setTotalRecordCount(request.getPaging().getTotalRecordCount());
pagingReq.setDescriptor("APPS.XXTG_EF_MO_WS_PAGING");