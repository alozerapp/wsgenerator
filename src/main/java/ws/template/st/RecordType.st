package xxtg.ws.model;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.sql.Struct;
import javax.xml.bind.annotation.XmlElement;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import tr.com.partnera.util.OCF;
import oracle.sql.BLOB;
import java.util.Date;


public class $class_name$ 
    extends UDT_BASE
{
    $variables$

    public void readSQL(SQLInput stream, String typeName)
    throws SQLException
    {
      Array array = null;
      Object object = null;
      $read_sql$
    }
    
    public void writeSQL(SQLOutput stream)
    throws SQLException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public Struct createStruct(Connection c) throws SQLException 
    {
        StructDescriptor structDesc = StructDescriptor.createDescriptor("APPS.$class_name$", c);
		
        $struct$

        STRUCT struct = new STRUCT(structDesc, c, attributes);
		return struct;
    }
	
	private BLOB getBlob(byte[] str, Connection con) throws SQLException, IOException { 

        BLOB blob = BLOB.createTemporary(con, true, BLOB.DURATION_SESSION); 
        blob.open(BLOB.MODE_READWRITE); 
        OutputStream writer = blob.getBinaryOutputStream(); 
        writer.write(str); 
        writer.flush(); 
        writer.close(); 
        blob.close(); 

        return blob; 
	} 

    @Override
    public Array createArray(Connection cnctn) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
	$get_set_methods$
}
