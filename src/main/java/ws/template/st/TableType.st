package xxtg.ws.model;

import java.sql.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;


public class $class_name$ 
    extends UDT_BASE
{
    private $variable_type$[] $variable_name$ = null;

    public $variable_type$[] get$variable_method$() {
        return $variable_name$;
    }

    public void set$variable_method$($variable_type$[] $variable_name$) {
        this.$variable_name$ = $variable_name$;
    }
    
    public $class_name$() {}

    public $class_name$(Array array)
    throws SQLException
    {
        List<$variable_type$> list = new ArrayList();
        if (array == null)
        {
            this.$variable_name$ = new $variable_type$[0];
            return;
        }
        ResultSet rs = array.getResultSet();
        while (rs.next())
        {
            int index = rs.getInt(1);
            list.add(($variable_type$)rs.getObject(2));
        }
        this.$variable_name$ = new $variable_type$[list.size()];
        list.toArray(this.$variable_name$);
    }

    @Override
    public Struct createStruct(Connection c) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Array createArray(Connection c) 
            throws SQLException 
    {
        ArrayDescriptor arrDesc = ArrayDescriptor.createDescriptor("APPS.$class_name$", c);
        if (this.$variable_name$ == null) 
        {
            return new ARRAY(arrDesc, c, new Struct[0]);
        }
        $array_struct_content$
        return array;
    }
}
