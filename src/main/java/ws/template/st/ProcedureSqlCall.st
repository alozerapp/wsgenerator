$map$
String sql = "begin $initialize_sql_call$ $package_name$.$procedure_name$ $question_marks$; end;";
OracleCallableStatement ocs = (OracleCallableStatement)conn.prepareCall(sql);
$set_parameters$	
ocs.executeQuery();
