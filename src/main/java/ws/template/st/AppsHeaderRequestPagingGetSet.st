@Override
public RequestHeader getRequestHeader()
{
	return this.requestHeader;
}

@Override
public void setRequestHeader(RequestHeader rh)
{
	this.requestHeader = rh;
}

@Override
public Filter getFilter()
{
	return this.filter;
}

@Override
public void setFilter(Filter filter)
{
	this.filter = filter;
}

@Override
public Sorting getSorting()
{
	return this.sorting;
}

@Override
public void setSorting(Sorting srtng)
{
	this.sorting = srtng;
}

@Override
public Paging getPaging()
{
	return this.paging;
}

@Override
public void setPaging(Paging paging)
{
	this.paging = paging;
}