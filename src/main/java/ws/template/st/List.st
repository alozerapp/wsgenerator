package xxtg.ws.model;

import java.math.BigDecimal;
import java.util.List;

public class $class_name$ {
    private List<$parameter_name_class$> $parameter_name$;

    public List<$parameter_name_class$> get$param_name_camel_case$() {
        return $parameter_name$;
    }

    public void set$param_name_camel_case$(List<$parameter_name_class$> $parameter_name$) {
        this.$parameter_name$ = $parameter_name$;
    }
}
