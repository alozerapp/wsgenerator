if(request.get$param_name_camel_case$() == null)
{
	ArrayDescriptor arrDesc = ArrayDescriptor.createDescriptor("APPS.$parameter_type_upper$", conn);

	ARRAY $parameter_name_lower$Array = new ARRAY(arrDesc, conn, new Struct[0]);
	ocs.setObject("$parameter_name$", $parameter_name_lower$Array);
}else
{
	ocs.setObject("$parameter_name$", request.get$param_name_camel_case$().createArray(conn));
}