@WebMethod(operationName = "$web_method_name$")
public $response_class_name$ $web_method_name$(@WebParam(name = "request") $request_class_name$ request)
	throws GenericWSFault
{
	$define_apps_header$
		
	Connection conn = null;    
	try
	{
		conn = getConnection();
		
		$web_method_content$
	   
	   conn.close();
	   $set_apps_header$
	   return response;
   }
	catch(Exception e){
		try{
			if(conn != null)
				conn.close();
		 } catch (Exception ee)
		 {;}
		  
		StackTrace stack = new StackTrace();
		java.util.List<String> lines = new java.util.ArrayList<String>();
		for(StackTraceElement element:e.getStackTrace()){
			lines.add(element.toString());
		}
		stack.setLines(lines);
		
		GenericWSFault fault = new GenericWSFault("WS Error");
		fault.setDetailedMessage(e.getMessage());
		fault.setStackTraceElement(stack);
		
		throw fault;
	}
}