package xxtg.ws;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.sql.DataSource;
import javax.xml.ws.WebServiceContext;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import xxtg.ws.fault.GenericWSFault;
import xxtg.ws.fault.StackTrace;
import xxtg.ws.model.AppsHeader;
import java.util.Date;
import xxtg.ws.model.*;
$imports$

@WebService(serviceName = "$web_service_name$")
@HandlerChain(file = "handler-chain.xml")    
public class $web_service_name$ {
	@Resource(mappedName = "$data_source_name$")
    protected DataSource ds;
	
	private Connection getConnection() throws SQLException {
        
        return ds.getConnection();
    }
	
	$web_methods_content$

}