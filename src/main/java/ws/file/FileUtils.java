/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.List;
import ws.WSGenerator;
import ws.WSUtils;
import ws.model.*;
import ws.template.TemplateUtils;



/**
 *
 * @author EXT0126117
 */
public class FileUtils {
    
    public String getPropertiesContent(String userDB, String passwordDB, String hostDB, String portDB, String sidDB) throws IOException
    {
        TemplateUtils templateUtils = new TemplateUtils();
        String script = "";
        /*if("profile".equals(ldtType))
        {
            script = templateUtils.getProfileScriptContent(helper.getTemplatesDirectory(), name, appShortName, appsPassword);
        }else if("message".equals(ldtType))
        {
            script = templateUtils.getMessageScriptContent(helper.getTemplatesDirectory(), name, appShortName, appsPassword);
        }*/
        script = templateUtils.getPropertiesContent(userDB, passwordDB, hostDB, portDB, sidDB, getTemplatesDirectory());
        
        return script;
    }
    
    public void createPropertiesFile(String objectFileName, String extention, String content)
    throws IOException
    {
        //Helper helper = new Helper();
        String path = getCurrent()+"\\"+objectFileName+"."+extention;
        File objectFile = new File(path);
        //objectFile.getParentFile().mkdirs();
        objectFile.createNewFile();
        
        BufferedWriter outputReq = new BufferedWriter(new FileWriter(objectFile));
        outputReq.write(content);
        outputReq.close();
    }
    
    public String getPropertiesFilePath()
    throws IOException
    {
        String current = getCurrent()+"wsGenProperties.properties";
        
        return current;
    }
    
    public String getPropertiesFilePathInside()
    throws IOException
    {
        String current = "ws/file/"+getPropertiesFileName()+"."+getPropertiesFileExteintion();
        
        return current;
    }
    
    public String getPropertiesFileName()
    {
        String current = "wsGenProperties";
        
        return current;
    }
    
    public String getPropertiesFileExteintion()
    {
        String current = "properties";
        
        return current;
    }
    
    public String getCcExcellFileName()
    {
        return "CcResults.xls";
    }
    
    public void createCcExcellFile()
    throws IOException
    {
        String path = getWSLogPath()+"\\"+getCcExcellFileName();
        File ccExcellFile = new File(path);
        ccExcellFile.getParentFile().mkdirs();
        ccExcellFile.createNewFile();
    }
    
    
    
    public void createLogFile()
    throws IOException
    {
        String path = getWSLogPath()+"\\"+getlogFileName();
        File logFile = new File(path);
        logFile.getParentFile().mkdirs();
        logFile.createNewFile();
    }
    
    public void writeLogFile(String logStr)
    {
        try{
            File logFile = new File(getWSLogPath()+"\\"+getlogFileName());
            FileWriter fileWritter = new FileWriter(logFile.getName(),true);
            BufferedWriter outputReq = new BufferedWriter(fileWritter);
            outputReq.write("\n"+logStr);
            outputReq.close();
        }catch(IOException exp)
        {
            ;
        }
        
    }
    
    
    public void createHandlerChainFile(String packageName)
    throws IOException
    {
        String path = getWSPath(packageName) +getHandlerChainFileName();
        File handlerChainFile = new File(path);
        handlerChainFile.getParentFile().mkdirs();
        handlerChainFile.createNewFile();
        
        writeHandlerChainFile(handlerChainFile);
    }
    
    public void createWebServiceFile(boolean appsHeaderFlag, String packageName, String fileName, String dataSourceName, List<Procedure> procedures)
    throws IOException
    {
        //String current = new java.io.File( "." ).getCanonicalPath();
        String path = getWSPath(packageName) +fileName+getJavaExtention();
        
        File webServiceFile = new File(path);
        webServiceFile.getParentFile().mkdirs();
        webServiceFile.createNewFile();
        
        //WebServiceContent webServiceContent = templateUtils.getWebServiceContent(templateUtils.getTemplatesDirectory(), packageName, dataSourceName, procedures, getRequestPrefix(), getResponsePrefix());
        writeWebServiceFile(appsHeaderFlag, webServiceFile, packageName, dataSourceName, procedures);
    }
    
    public void createRequestResponseFile(boolean appsHeaderFlag, String packageName, String procedureName, List<Parameter> parameters)
    throws IOException
    {
        String requestFileName = procedureName+getRequestExtension();
        String responseFileName = procedureName+getResponseExtension();
        String requestClassName = procedureName+getRequestPrefix();
        String responseClassName = procedureName+getResponsePrefix();
        //String current = new java.io.File( "." ).getCanonicalPath();
        String path = getWSModelPath(packageName);
        String templatesDirectory = getTemplatesDirectory();
        
        //RequestResponseFileContent requestResponseFileContent = templateUtils.getRequestResponseFileContent(templatesDirectory, requestClassName, responseClassName, parameters);
        //System.out.println("Current relative path is: " + current);
        //System.out.println("path is: " + path);
        File requestFile = new File(path+requestFileName);
        requestFile.getParentFile().mkdirs();
        requestFile.createNewFile();
        writeRequestFileContent(appsHeaderFlag, requestFile, templatesDirectory, requestFileName, requestClassName, parameters);
        
        File responseFile = new File(path+responseFileName);
        responseFile.getParentFile().mkdirs();
        responseFile.createNewFile();
        //writeResponseFileContent(requestResponseFileContent, responseFile, templatesDirectory, responseFileName, parameters);
        writeResponseFileContent(appsHeaderFlag, responseFile, templatesDirectory, responseFileName, responseClassName, parameters);
    }
    
    public void createListFiles(String templatesDirectory, Parameter parameter, String packageName)
    throws IOException
    {
        String path = getWSModelPath(packageName);
        File listLineFile = new File(path+parameter.getParameterNameJava()+getJavaExtention());
        listLineFile.getParentFile().mkdirs();
        listLineFile.createNewFile();
        
        /*
         * sys_refcursor yazilacagi zaman burasi doldurulacak.
         */
        writeListLineFile(listLineFile, templatesDirectory, parameter.getParameterNameJava());
        
        File listFile = new File(path+parameter.getParameterNameJava()+"_LIST"+getJavaExtention());
        listFile.getParentFile().mkdirs();
        listFile.createNewFile();
        
        writeListFile(listFile, templatesDirectory, parameter);
    }
    
    public void createTypeFiles(Procedure procedure, String packageName)
    throws IOException
    {
        String templatesDirectory = getTemplatesDirectory();
        
        for (Parameter parameter : procedure.getParamters()) 
        {
            if("Object".equals(parameter.getParameterTypeJava()))
            {
                for (Type type : parameter.getTypes()) 
                {
                    if(type.isCustomType())
                    {
                        createTypeFile(templatesDirectory, packageName, type);
                    }
                }
            }
            
        }
     
    }
    
    public void createTypeFile(String templatesDirectory, String packageName, Type type)
    throws IOException
    {
        if(!"xxtg_ef_mo_ws_paging".equals(type.getTypeName()))
        {
            String path = getWSModelPath(packageName);
            File typeFile = new File(path+type.getTypeName().toUpperCase()+getJavaExtention());
            typeFile.getParentFile().mkdirs();
            typeFile.createNewFile();
            
            writeTypeFile(typeFile, packageName, type);
        }
    }
    
    public void writeTypeFile(File typeFile, String packageName, Type type)
    throws IOException
    {
        TemplateUtils templateUtils = new TemplateUtils();
        String typeFileContentStr = templateUtils.getTypeFileContent(getTemplatesDirectory(), packageName, type);
        BufferedWriter outputReq = new BufferedWriter(new FileWriter(typeFile));
        outputReq.write(typeFileContentStr);
        outputReq.close();
    }
    
    
    
    public void writeHandlerChainFile(File webServiceFile)
    throws IOException
    {
        TemplateUtils templateUtils = new TemplateUtils();
        String handlerChainFileStr = templateUtils.getHandlerChainContent(getTemplatesDirectory());
        BufferedWriter outputReq = new BufferedWriter(new FileWriter(webServiceFile));
        outputReq.write(handlerChainFileStr);
        outputReq.close();
    }
    
    public void writeWebServiceFile(boolean appsHeaderFlag, File webServiceFile, String packageName, String dataSourceName, List<Procedure> procedures)
    throws IOException
    {
        TemplateUtils templateUtils = new TemplateUtils();
        String webSericeFileContentStr = templateUtils.getWebServiceContent(appsHeaderFlag, getTemplatesDirectory(), packageName, dataSourceName, procedures, getRequestPrefix(), getResponsePrefix(), getListFileAnnotaiton());
        BufferedWriter outputReq = new BufferedWriter(new FileWriter(webServiceFile));
        outputReq.write(webSericeFileContentStr);
        outputReq.close();
    }
    
    public void writeRequestFileContent(boolean appsHeaderFlag, File requestFile, String templatesDirectory, String requestFileName, String requestClassName, List<Parameter> parameters)
    throws IOException
    {
        TemplateUtils templateUtils = new TemplateUtils();
        String requestFileContentStr = templateUtils.getRequestFileContent(appsHeaderFlag, templatesDirectory, requestClassName, parameters);
        BufferedWriter outputReq = new BufferedWriter(new FileWriter(requestFile));
        outputReq.write(requestFileContentStr);
        outputReq.close();
    }
    
    public void writeResponseFileContent(boolean appsHeaderFlag, File responseFile, String templatesDirectory, String responseFileName, String responseClassName, List<Parameter> parameters)
    throws IOException
    {
        TemplateUtils templateUtils = new TemplateUtils();
        String responseFileContentStr = templateUtils.getResponseFileContent(appsHeaderFlag, templatesDirectory, responseClassName, parameters, getListFileAnnotaiton());
        BufferedWriter outputReq = new BufferedWriter(new FileWriter(responseFile));
        outputReq.write(responseFileContentStr);
        outputReq.close();
    }   
    
    public void writeListLineFile(File listLineFile, String templatesDirectory, String paramterName)
    throws IOException
    {
        /*
         * sys_refcursor yazilacagi zaman burasi doldurulacak.
         */
        TemplateUtils templateUtils = new TemplateUtils();
        String listLineFileStr = templateUtils.getListLineContent(templatesDirectory, paramterName);
        BufferedWriter outputReq = new BufferedWriter(new FileWriter(listLineFile));
        outputReq.write(listLineFileStr);
        outputReq.close();
        
    }
    
    public void writeListFile(File listFile, String templatesDirectory, Parameter parameter)
    throws IOException
    {
        /*
         * sys_refcursor yazilacagi zaman burasi doldurulacak.
         */
        TemplateUtils templateUtils = new TemplateUtils();
        String listFileStr = templateUtils.getListContent(templatesDirectory, parameter);
        BufferedWriter outputReq = new BufferedWriter(new FileWriter(listFile));
        outputReq.write(listFileStr);
        outputReq.close();
        
    }
    
    public void deleteAll()
    throws IOException
    {
        File rootDirectory = new File(getProjectPath());
        if (rootDirectory.exists())
        {
            deleteFolder(rootDirectory);
        }
    }
    
    public void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if(files!=null) { //some JVMs return null for empty dirs
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }
    
    public String getHandlerChainFileName()
    {
        return "handler-chain.xml";
    }
    
    public String getlogFileName()
    {
        WSUtils utils = new WSUtils();
        return utils.getProjectName()+"Log.txt";
    }
    
    public String getRequestPrefix()
    {
        return "Request";
    }
    
    public String getResponsePrefix()
    {
        return "Response";
    }
    
    public String getRequestExtension()
    {
        return getRequestPrefix()+getJavaExtention();
    }
    
    public String getResponseExtension()
    {
        return getResponsePrefix()+getJavaExtention();
    }
    
    public String getCurrent()
    throws IOException
    {
        //String current1 = new java.io.File( "." ).getCanonicalPath();
        /*
         * development(debug) directory
         */
        String current = "";
        if(getClass().getResource("").getPath().lastIndexOf(".jar") >= 0)
        {
            /*
             * executable jar directory
             */
            current = getClass().getResource("").getPath();
            System.out.println("current: "+current);
            current = current.substring(0, current.lastIndexOf("!"));
            System.out.println("current2: "+current);
            current = current.substring(0, current.lastIndexOf("/")+1);
            System.out.println("current3: "+current);
            if(current.indexOf("file:/") >= 0)
            {
                current = current.substring(6, current.length());
                System.out.println("current4: "+current);
            }
        }else
        {
            /*
             * development(debug) directory
             */
            current = getClass().getResource("").getPath().substring(1, getClass().getResource("").getPath().lastIndexOf("target"));
        }
        
        

        //writeLogFile("Currrent Dir: "+current);
        
        /*if(current.substring(current.length()-1, current.length()).equals("/"))
        {
            current = current.substring(0, current.length()-1);
        }*/
        
        
        return current;
    }
    
    public String getJavaExtention()
    {
        return ".java";
    }
    
    public String getListFileAnnotaiton()
    {
        return "LIST";
    }
    
    public String getProjectPath()
    throws IOException     
    {
        WSUtils utils = new WSUtils();
        return getCurrent()+"\\"+utils.getProjectName()+"\\";
    }
    
    public String getPackagePath(String packageName)
    throws IOException     
    {
        WSUtils utils = new WSUtils();
        return getCurrent()+"\\"+utils.getProjectName()+"\\"+packageName+"\\";
    }
    
    public String getWSPath(String packageName)
    throws IOException     
    {
        WSUtils utils = new WSUtils();
        return getCurrent()+"\\"+utils.getProjectName()+"\\"+packageName+"\\src\\java\\xxtg\\ws\\";
    }
    
    public String getWSModelPath(String packageName)
    throws IOException        
    {
        WSUtils utils = new WSUtils();
        return getCurrent()+"\\"+utils.getProjectName()+"\\"+packageName+"\\src\\java\\xxtg\\ws\\model\\";
    }
    
    public String getWSLogPath()
    throws IOException        
    {
        WSUtils utils = new WSUtils();
        return getCurrent();
    }
    
    public String getTemplatesDirectory()
    throws IOException
    {
        /*
         * executable jar directory
         */
        /*WSGenerator g = new WSGenerator();
        //URL url = g.getClass().getClassLoader().getResource("ws/template/st");
        URL url = this.getClass().getClassLoader().getResource("ws/template/st/");
        String fullPath = getClass().getClassLoader().getResource("ws/template/st/AppsHeader.st").getFile();  
        System.out.println("url0: "+url.getPath());
        System.out.println("url1: "+fullPath);
        
        URL url2 = this.getClass().getClassLoader().getResource("ws/template/st/AppsHeader.st");
        System.out.println("url2: "+url2.getFile());*/
        
        //String current = url.getPath();
        String current = "ws/template/st/";
        
        
        return current;
    }
}
