/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.model;

import java.util.List;

/**
 *
 * @author EXT0126117
 */
public class Parameter {
    private String parameterName;
    private String parameterType;
    private String parameterDirection;
    private String parameterNameJava;
    private String parameterTypeJava;
    private String parameterTypeJavaMethod;
    private String parameterNameJavaMethod;
    private List<Type> types;

    /**
     * @return the parameterName
     */
    public String getParameterName() {
        return parameterName;
    }

    /**
     * @param parameterName the parameterName to set
     */
    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    /**
     * @return the parameterType
     */
    public String getParameterType() {
        return parameterType;
    }

    /**
     * @param parameterType the parameterType to set
     */
    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    /**
     * @return the parameterDirection
     */
    public String getParameterDirection() {
        return parameterDirection;
    }

    /**
     * @param parameterDirection the parameterDirection to set
     */
    public void setParameterDirection(String parameterDirection) {
        this.parameterDirection = parameterDirection;
    }

    /**
     * @return the parameterNameJava
     */
    public String getParameterNameJava() {
        return parameterNameJava;
    }

    /**
     * @param parameterNameJava the parameterNameJava to set
     */
    public void setParameterNameJava(String parameterNameJava) {
        this.parameterNameJava = parameterNameJava;
    }

    /**
     * @return the parameterTypeJava
     */
    public String getParameterTypeJava() {
        return parameterTypeJava;
    }

    /**
     * @param parameterTypeJava the parameterTypeJava to set
     */
    public void setParameterTypeJava(String parameterTypeJava) {
        this.parameterTypeJava = parameterTypeJava;
    }

    /**
     * @return the parameterTypeJavaMethod
     */
    public String getParameterTypeJavaMethod() {
        return parameterTypeJavaMethod;
    }

    /**
     * @param parameterTypeJavaMethod the parameterTypeJavaMethod to set
     */
    public void setParameterTypeJavaMethod(String parameterTypeJavaMethod) {
        this.parameterTypeJavaMethod = parameterTypeJavaMethod;
    }

    /**
     * @return the parameterNameJavaMethod
     */
    public String getParameterNameJavaMethod() {
        return parameterNameJavaMethod;
    }

    /**
     * @param parameterNameJavaMethod the parameterNameJavaMethod to set
     */
    public void setParameterNameJavaMethod(String parameterNameJavaMethod) {
        this.parameterNameJavaMethod = parameterNameJavaMethod;
    }

    /**
     * @return the types
     */
    public List<Type> getTypes() {
        return types;
    }

    /**
     * @param types the types to set
     */
    public void setTypes(List<Type> types) {
        this.types = types;
    }


}
