/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.model;

import java.util.List;

/**
 *
 * @author EXT0126117
 */
public class TypeColumn {
    private String columnName;
    private String columnType;
    private String columnNameJava;
    private String columnTypeJava;
    private String columnTypeJavaMethod;
    private String columnNameJavaMethod;
    private boolean customType;
    private String columnTypeClass;
 
    /**
     * @return the columnName
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * @param columnName the columnName to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    /**
     * @return the columnType
     */
    public String getColumnType() {
        return columnType;
    }

    /**
     * @param columnType the columnType to set
     */
    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    /**
     * @return the columnNameJava
     */
    public String getColumnNameJava() {
        return columnNameJava;
    }

    /**
     * @param columnNameJava the columnNameJava to set
     */
    public void setColumnNameJava(String columnNameJava) {
        this.columnNameJava = columnNameJava;
    }

    /**
     * @return the columnTypeJava
     */
    public String getColumnTypeJava() {
        return columnTypeJava;
    }

    /**
     * @param columnTypeJava the columnTypeJava to set
     */
    public void setColumnTypeJava(String columnTypeJava) {
        this.columnTypeJava = columnTypeJava;
    }

    /**
     * @return the columnTypeJavaMethod
     */
    public String getColumnTypeJavaMethod() {
        return columnTypeJavaMethod;
    }

    /**
     * @param columnTypeJavaMethod the columnTypeJavaMethod to set
     */
    public void setColumnTypeJavaMethod(String columnTypeJavaMethod) {
        this.columnTypeJavaMethod = columnTypeJavaMethod;
    }

    /**
     * @return the columnNameJavaMethod
     */
    public String getColumnNameJavaMethod() {
        return columnNameJavaMethod;
    }

    /**
     * @param columnNameJavaMethod the columnNameJavaMethod to set
     */
    public void setColumnNameJavaMethod(String columnNameJavaMethod) {
        this.columnNameJavaMethod = columnNameJavaMethod;
    }

    /**
     * @return the customType
     */
    public boolean isCustomType() {
        return customType;
    }

    /**
     * @param customType the customType to set
     */
    public void setCustomType(boolean customType) {
        this.customType = customType;
    }

    /**
     * @return the columnTypeClass
     */
    public String getColumnTypeClass() {
        return columnTypeClass;
    }

    /**
     * @param columnTypeClass the columnTypeClass to set
     */
    public void setColumnTypeClass(String columnTypeClass) {
        this.columnTypeClass = columnTypeClass;
    }
}
