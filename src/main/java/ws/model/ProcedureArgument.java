/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.model;

/**
 *
 * @author EXT0126117
 */
public class ProcedureArgument {
    private String owner;
    private String objectName;
    private String objectType;
    private String packageName;
    private int subprogramId;
    private String argumentName;
    private int positon;
    private int sequence;
    private String dataType;
    private String inOut;
    private String typeOwner;
    private String typeName;

    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return the objectName
     */
    public String getObjectName() {
        return objectName;
    }

    /**
     * @param objectName the objectName to set
     */
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    /**
     * @return the objectType
     */
    public String getObjectType() {
        return objectType;
    }

    /**
     * @param objectType the objectType to set
     */
    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    /**
     * @return the packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName the packageName to set
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return the subprogramId
     */
    public int getSubprogramId() {
        return subprogramId;
    }

    /**
     * @param subprogramId the subprogramId to set
     */
    public void setSubprogramId(int subprogramId) {
        this.subprogramId = subprogramId;
    }

    /**
     * @return the argumentName
     */
    public String getArgumentName() {
        return argumentName;
    }

    /**
     * @param argumentName the argumentName to set
     */
    public void setArgumentName(String argumentName) {
        this.argumentName = argumentName;
    }

    /**
     * @return the positon
     */
    public int getPositon() {
        return positon;
    }

    /**
     * @param positon the positon to set
     */
    public void setPositon(int positon) {
        this.positon = positon;
    }

    /**
     * @return the sequence
     */
    public int getSequence() {
        return sequence;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    /**
     * @return the dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * @param dataType the dataType to set
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * @return the inOut
     */
    public String getInOut() {
        return inOut;
    }

    /**
     * @param inOut the inOut to set
     */
    public void setInOut(String inOut) {
        this.inOut = inOut;
    }

    /**
     * @return the typeOwner
     */
    public String getTypeOwner() {
        return typeOwner;
    }

    /**
     * @param typeOwner the typeOwner to set
     */
    public void setTypeOwner(String typeOwner) {
        this.typeOwner = typeOwner;
    }

    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName the typeName to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

}
