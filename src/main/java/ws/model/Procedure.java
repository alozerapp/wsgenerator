/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.model;

import ws.model.Parameter;
import java.util.List;

/**
 *
 * @author EXT0126117
 */
public class Procedure {
    private String procedureName;
    private List<Parameter> paramters; 
    private String procedureScript;

    /**
     * @return the procedureName
     */
    public String getProcedureName() {
        return procedureName;
    }

    /**
     * @param procedureName the procedureName to set
     */
    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    /**
     * @return the paramters
     */
    public List<Parameter> getParamters() {
        return paramters;
    }

    /**
     * @param paramters the paramters to set
     */
    public void setParamters(List<Parameter> paramters) {
        this.paramters = paramters;
    }

    /**
     * @return the procedureScript
     */
    public String getProcedureScript() {
        return procedureScript;
    }

    /**
     * @param procedureScript the procedureScript to set
     */
    public void setProcedureScript(String procedureScript) {
        this.procedureScript = procedureScript;
    }
}
