/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.model;

import java.util.List;

/**
 *
 * @author EXT0126117
 */
public class Type {
    private int index;
    private String script;
    private String typeName;
    private String typeClass;
    private List<TypeColumn> typeColumns;
    private boolean customType;
    

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return the script
     */
    public String getScript() {
        return script;
    }

    /**
     * @param script the script to set
     */
    public void setScript(String script) {
        this.script = script;
    }

    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName the typeName to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * @return the typeColumns
     */
    public List<TypeColumn> getTypeColumns() {
        return typeColumns;
    }

    /**
     * @param typeColumns the typeColumns to set
     */
    public void setTypeColumns(List<TypeColumn> typeColumns) {
        this.typeColumns = typeColumns;
    }

    /**
     * @return the typeClass
     */
    public String getTypeClass() {
        return typeClass;
    }

    /**
     * @param typeClass the typeClass to set
     */
    public void setTypeClass(String typeClass) {
        this.typeClass = typeClass;
    }

    /**
     * @return the customType
     */
    public boolean isCustomType() {
        return customType;
    }

    /**
     * @param customType the customType to set
     */
    public void setCustomType(boolean customType) {
        this.customType = customType;
    }
}
