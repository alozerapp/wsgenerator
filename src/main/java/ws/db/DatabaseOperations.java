/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ws.db;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ws.file.FileUtils;
import ws.model.Parameter;
import ws.model.ProcedureArgument;

/**
 *
 * @author EXT0126117
 */
public class DatabaseOperations {
    private String user;
    private String password;
    private String host;
    private String port;
    private String serviceName;
    
    public DatabaseOperations()
    {
        
    }
    
    public DatabaseOperations(String user, String password, String host, String port, String serviceName)
    {
        this.user = user;
        this.password = password;
        this.host = host;
        this.port = port;
        this.serviceName = serviceName;
    }
    
    public Connection getConnection() throws SQLException {
        FileUtils fileUtils = new FileUtils();
        Connection conn;
        try {
            
            //fileUtils.writeLogFile("Connection 1");
            
            Class.forName("oracle.jdbc.driver.OracleDriver");
            
            //fileUtils.writeLogFile("Connection 2");
            
            String connectionString = "jdbc:oracle:thin:@"+getHost()+":"+getPort()+":"+getServiceName();
            
            //fileUtils.writeLogFile("Connection 3");
            
            conn = DriverManager.getConnection(connectionString, getUser(), getPassword());
            
            //fileUtils.writeLogFile("Connection 4");
            
                //conn = DriverManager.getConnection("jdbc:oracle:thin:@panda.erp.mis.turkcell.tgc:1533:TDEV", "APPS", "devapps#490");
        }
        catch (Throwable ex) {
            Logger.getLogger(DatabaseOperations.class.getName()).log(Level.SEVERE, null, ex);
            conn = null;
            
            fileUtils.writeLogFile("Connection Error in DatabaseOperations class:  "+ex.toString());
            
            //throw new ClassNotFoundException("Error: OracleDriver Class not found!");
        }
        
        
        //fileUtils.writeLogFile("Connection 5");
            
        
        return conn;
    }
    
    
    public Clob getPksScriptFromDb(Connection conn, String packageName) throws SQLException{
        String sqlStatement = "select dbms_metadata.get_ddl('PACKAGE_SPEC', '" +packageName+"') pks_script from dual";
        //OracleCallableStatement cs = (OracleCallableStatement)conn.prepareCall(sqlStatement);

        CallableStatement cs = conn.prepareCall(sqlStatement);
        ResultSet rs = cs.executeQuery();
        rs.next();
        
        Clob packageSpec = rs.getClob("pks_script");

        return packageSpec;
    }
    
    public Clob getPkbScriptFromDb(Connection conn, String packageName) throws SQLException{
        String sqlStatement = "select dbms_metadata.get_ddl('PACKAGE_BODY', '" +packageName+"') pkb_script from dual";
        //OracleCallableStatement cs = (OracleCallableStatement)conn.prepareCall(sqlStatement);

        CallableStatement cs = conn.prepareCall(sqlStatement);
        ResultSet rs = cs.executeQuery();
        rs.next();
        
        Clob packageSpec = rs.getClob("pkb_script");

        return packageSpec;
    }
    
    public List<ProcedureArgument> getPackageArguments(Connection conn, String packageName) throws SQLException{
        List<ProcedureArgument> procedureArgumentList = new ArrayList<ProcedureArgument>();
        
        String objectNameTemp = "";
        String objectTypeTemp = "";
        String sqlStatement = "select owner, object_name, package_name, subprogram_id, argument_name, position, sequence, data_type, in_out, type_owner, type_name from dba_arguments ";
        sqlStatement = sqlStatement +" where package_name = '"+packageName+"' order by subprogram_id , sequence "; 
        //OracleCallableStatement cs = (OracleCallableStatement)conn.prepareCall(sqlStatement);

        CallableStatement cs = conn.prepareCall(sqlStatement);
        ResultSet rs = cs.executeQuery();
        while(rs.next())
        {
            ProcedureArgument procedureArgument = new ProcedureArgument();
            procedureArgument.setOwner(rs.getString("owner"));
            procedureArgument.setObjectName(rs.getString("object_name"));
            
            // first row
            if(objectNameTemp.equals(""))
            {
                objectNameTemp = rs.getString("object_name");
                if(rs.getInt("position") == 0)
                {
                    objectTypeTemp = "FUNCTION";
                }else
                {
                    objectTypeTemp = "PROCEDURE";
                }
            }
            
            if(!objectNameTemp.equals(rs.getString("object_name")))
            {
                //objectNameTemp = rs.getString("object_name");
                if(rs.getInt("position") == 0)
                {
                    objectTypeTemp = "FUNCTION";
                }else
                {
                    objectTypeTemp = "PROCEDURE";
                }
                
            }
            procedureArgument.setObjectType(objectTypeTemp);
            procedureArgument.setPackageName(rs.getString("package_name"));
            procedureArgument.setSubprogramId(rs.getInt("subprogram_id"));
            procedureArgument.setArgumentName(rs.getString("argument_name"));
            procedureArgument.setPositon(rs.getInt("position"));
            procedureArgument.setSequence(rs.getInt("sequence"));
            procedureArgument.setDataType(rs.getString("data_type"));
            procedureArgument.setInOut(rs.getString("in_out"));
            procedureArgument.setTypeOwner(rs.getString("type_owner"));
            procedureArgument.setTypeName(rs.getString("type_name"));
            
            procedureArgumentList.add(procedureArgument);
        }

        return procedureArgumentList;
    }
    
    public boolean isType(Connection conn, String parameterType) throws SQLException{
        String sqlStatement = " select case when count(*) > 0 then 'Y' else 'N'end is_type from dba_objects "+
                              " where object_name = '"+parameterType.toUpperCase()+"' "+
                              " and object_type = 'TYPE' "+
                              " and owner = 'APPS' ";
        //OracleCallableStatement cs = (OracleCallableStatement)conn.prepareCall(sqlStatement);

        CallableStatement cs = conn.prepareCall(sqlStatement);
        ResultSet rs = cs.executeQuery();
        rs.next();
        
        String isType = rs.getString("is_type");
        
        if("Y".equals(isType))
        {
            return true;
        }else
        {
            return false;
        }
    }
    
    public Clob getTypeScriptFromDb(Connection conn, String parameterType) {
        try
        {
            System.out.println("parameterType :"+parameterType);
            String sqlStatement = "select dbms_metadata.get_ddl('TYPE', '" +parameterType+"') type_script from dual";
            //OracleCallableStatement cs = (OracleCallableStatement)conn.prepareCall(sqlStatement);

            CallableStatement cs = conn.prepareCall(sqlStatement);
            ResultSet rs = cs.executeQuery();
            rs.next();

            Clob typeScript = rs.getClob("type_script");

            return typeScript;
        }catch(SQLException exp)
        {
            System.out.println("Error: "+exp.toString());
            return null;
        }
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    
}
