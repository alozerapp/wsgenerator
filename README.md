## WSGenerator

WSGenerator is an executable client gui which generates JAVA codes for weblogic webservice applications from oracle database packages.

After generating JAVA code using WSGenerator you will need [libraries(jars)](/requiredlib) to compile and produce a war file.

## Screenshot

Screenshot software

[![solarized dualmode](/images/img0.jpg?raw=true)](#features)

[![solarized dualmode](/images/img1.jpg?raw=true)](#features)


## Download

* [Latest Version](https://gitlab.com/alozerapp/wsgenerator/raw/master/build/WSGenerator-1.5.jar)

Other Versions

## Contact
#### Developer
* e-mail: alozer@gmail.com



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
Prerequisites

git clone https://alozer1@gitlab.com/alozerapp/wsgenerator.git
cd wsgenerator